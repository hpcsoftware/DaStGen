#include <string>
#include <vector>
#include "../domain/DataRepresentation.cpp"

class ForwardAnnotationHandler {
private:
    const std::string forwardAnnotation = "forward";
    const std::string classNameFieldNameSeparator = "::";
    const std::string wildcardName = "*";

    bool isForwardAnnotation(Annotation *annotation) {
        bool isForwardAnnotation = !annotation->tokens.empty() && annotation->tokens[0] == forwardAnnotation;
        return isForwardAnnotation;
    }

    bool isClassForwardAnnotation(Annotation *annotation) {
        bool isClassForwardAnnotation = this->isForwardAnnotation(annotation)
                && annotation->tokens[2] != classNameFieldNameSeparator;
        return isClassForwardAnnotation;
    }

    bool isFieldForwardAnnotation(Annotation *annotation) {
        bool isClassForwardAnnotation = this->isForwardAnnotation(annotation)
                                        && annotation->tokens[2] == classNameFieldNameSeparator;
        return isClassForwardAnnotation;
    }

    std::string getTargetClassName(Annotation *annotation) {
        return annotation->tokens[1];
    }

    std::string getTargetFieldName(Annotation *annotation) {
        return annotation->tokens[3];
    }

    bool isAnnotationTargetClass(Annotation *annotation, DataClass *targetDataClass) {
        std::string targetClassName = this->getTargetClassName(annotation);
        if (targetClassName == wildcardName) return true;
        bool isTarget = targetClassName == targetDataClass->name;
        return isTarget;
    }

    void handleForwardClassAnnotation(Annotation *annotation, const std::vector<DataClass *>& derivedClasses) {
        bool haveAnyBeenFound = false;
        for (auto targetClass : derivedClasses) {
            if (!this->isAnnotationTargetClass(annotation, targetClass)) continue;
            Annotation *newAnnotation = new Annotation();
            newAnnotation->parentField = targetClass;
            newAnnotation->tokens.insert(newAnnotation->tokens.end(), annotation->tokens.begin() + 2, annotation->tokens.end());
            targetClass->annotations.emplace_back(newAnnotation);
            haveAnyBeenFound = true;
        }
        if (!haveAnyBeenFound) {
            std::cerr << "Unable to find the forward annotation target class" << std::endl;
        }
    }

    bool isAnnotationTargetField(Annotation *annotation, ClassField *targetField) {
        std::string targetFieldName = this->getTargetFieldName(annotation);
        if (targetFieldName == wildcardName) return true;
        bool isTarget = targetFieldName == targetField->name;
        return isTarget;
    }

    void handleForwardFieldAnnotation(Annotation *annotation, const std::vector<DataClass *>& derivedClasses) {
        bool haveAnyBeenFound = false;
        for (auto targetClass : derivedClasses) {
            if (!this->isAnnotationTargetClass(annotation, targetClass)) continue;
            for (auto targetField : targetClass->fields) {
                if (!this->isAnnotationTargetField(annotation, targetField)) continue;
                Annotation *newAnnotation = new Annotation();
                newAnnotation->parentField = targetField;
                newAnnotation->tokens.insert(newAnnotation->tokens.end(), annotation->tokens.begin() + 4, annotation->tokens.end());
                targetField->annotations.emplace_back(newAnnotation);
                haveAnyBeenFound = true;
            }
        }
        if (!haveAnyBeenFound) {
            std::cerr << "Unable to find the forward annotation target field" << std::endl;
        }
    }

public:

    void handleForwardAnnotations(DataClass *sourceDataClass, const std::vector<DataClass *>& targetDataClasses) {
        for (auto annotation : sourceDataClass->annotations) {
            if (!this->isForwardAnnotation(annotation)) continue;
            if (this->isClassForwardAnnotation(annotation)) {
                this->handleForwardClassAnnotation(annotation, targetDataClasses);
            } else if (this->isFieldForwardAnnotation(annotation)) {
                this->handleForwardFieldAnnotation(annotation, targetDataClasses);
            } else {
                std::cerr << "Unknown forward annotation" << std::endl;
            }
        }
    }

};
