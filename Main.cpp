#include "cli/CliArgsHandler.cpp"
#include "parser/clang/ClangSourceParserV3.cpp"
#include "transformation/TransformationHandler.cpp"
#include "generator/cpp/CppSourceGenerator.cpp"
#include "generator/SourceGenerator.cpp"
#include "writer/SourceWriter.cpp"

CliArgsHandler argsHandler;
ClangSourceParserV3 parser;
TransformationHandler transformationHandler;
CppSourceGenerator sourceGenerator;
SourceUnitWriter writer;

int main(int argc, char** argv) {
    argsHandler.parse(argc, argv);
    std::vector<DataClass *> dataClasses = parser.parseSourceFiles(argsHandler.argc(), argsHandler.argv());
    std::cout << "Parsed " + std::to_string(dataClasses.size()) << " classes" << std::endl;
    dataClasses = transformationHandler.handleTransformations(dataClasses);
    std::vector<SourceUnit *> sourceUnits = sourceGenerator.generateSourceUnits(dataClasses);
    writer.writeSourceUnits(std::vector<std::string> {argsHandler.getOutputPath()}, sourceUnits);
    std::cout << "Generated " + std::to_string(dataClasses.size()) << " classes" << std::endl;
}
