#include "../SourceGenerator.cpp"
#include "CppHeaderSourceUnitGenerator.cpp"
#include "CppImplSourceUnitGenerator.cpp"
#include "../../methods/MethodArgClassRefOptimiser.cpp"

class CppSourceGenerator : public ISourceGenerator,
                           private CppHeaderSourceUnitGenerator,
                           private CppImplSourceUnitGenerator {
private:
    MethodArgClassRefOptimiser classRefOptimiser;

public:
    std::vector<SourceUnit *> generateSourceUnits(DataClass *dataClass) override {
        classRefOptimiser.optimiseDataClass(dataClass);
        return std::vector<SourceUnit *> {
            this->generateHeaderSourceUnit(dataClass),
            this->generateImplSourceUnit(dataClass)
        };
    }

    std::vector<SourceUnit *> generateSourceUnits(const std::vector<DataClass*>& dataClasses) override {
        std::vector<SourceUnit *> sources;
        for (auto dataClass : dataClasses) {
            std::vector<SourceUnit *> classSources = this->generateSourceUnits(dataClass);
            sources.insert(sources.end(), classSources.begin(), classSources.end());
        }
        return sources;
    }
};
