#ifndef __DASTGEN2_SourceGenerator__
#define __DASTGEN2_SourceGenerator__

#include "../domain/DataRepresentation.cpp"
#include "../domain/SourceUnit.cpp"

class ISourceGenerator {
public:
    virtual std::vector<SourceUnit *> generateSourceUnits(DataClass* dataClass) = 0;
    virtual std::vector<SourceUnit *> generateSourceUnits(const std::vector<DataClass*>& dataClasses) = 0;

};

#endif
