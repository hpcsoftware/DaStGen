#include <iostream>
#include "../domain/DataRepresentation.cpp"
#include "GetterSetterGenerator.cpp"

#ifndef __DASTGEN2_ConstructorGenerator__
#define __DASTGEN2_ConstructorGenerator__

class ConstructorGenerator {
private:
    GetterSetterGenerator getterSetterGenerator;
    IStatement *setFieldViaValueAssignment(ClassField *classField, IExpression *value) {
        auto fieldRef = new ClassFieldVariableAccess(new LocalVariableAccess(classField->name));
        return new ValueAssignment(fieldRef, value);
    }
    IStatement *setFieldViaSetterInvocation(ClassField *classField, IExpression *value) {
        auto setter = getterSetterGenerator.getVariableSetter(classField);
        if (setter == nullptr) {
            std::cerr << "Cannot find class field setter" << std::endl;
        }
        return new ExpressionStatement(new ClassInstanceMethodInvocation(setter->name, std::vector<IExpression *> { value }));
    }

    std::string getConstructorArgNameForField(ClassField *classField) {
        return classField->name;
    }

public:
    ClassConstructor *generateNoArgsConstructor(DataClass *dataClass) {
        ClassConstructor *constructor = new ClassConstructor();
        constructor->parentClass = dataClass;
        return constructor;
    }

    ClassConstructor *generateAllArgsConstructor(DataClass *dataClass) {
        ClassConstructor *constructor = generateNoArgsConstructor(dataClass);
        for (auto field : dataClass->fields) {
            MethodArgument *argument = new MethodArgument();
            argument->name = getConstructorArgNameForField(field);
            argument->type = field->type;
            constructor->methodArguments.emplace_back(argument);
        }
        for (auto field : dataClass->fields) {
            IStatement *setterStmt = setFieldViaSetterInvocation(field, new LocalVariableAccess(getConstructorArgNameForField(field)));
            constructor->methodStatements.emplace_back(setterStmt);
        }
        return constructor;
    }

    ClassConstructor *generateCopyConstructor(DataClass *dataClass) {
        const std::string argName = "val";
        MethodArgument *arg = new MethodArgument();
        arg->name = argName;
        arg->type = new DataClassDataType(dataClass);
        std::vector<MethodArgument *> args = std::vector<MethodArgument *> {arg};
        std::vector<IStatement *> body;
        for (auto field : dataClass->fields) {
            auto thisFieldRef = new ClassFieldVariableAccess(new LocalVariableAccess(field->name));
            auto otherFieldRef = new ObjectFieldVariableAccess(new LocalVariableAccess(argName), field->name);
            body.emplace_back(new ValueAssignment(thisFieldRef, otherFieldRef));
        }
        ClassConstructor *constructor = generateNoArgsConstructor(dataClass);
        constructor->methodArguments = args;
        constructor->methodStatements = body;
        return constructor;
    }

    ClassConstructor *generateCopyConstructor(DataClass *target, DataClass* inputClass) {
        const std::string argName = "val";
        std::vector<IStatement *> body;
        for (auto setter : getterSetterGenerator.getSetters(target)) {
            auto inputGetter = getterSetterGenerator.getCorrespondingGetter(setter, inputClass);
            if (inputGetter == nullptr) continue;
            auto getterInvocation = new MethodInvocation(new LocalVariableAccess(argName), inputGetter->name, std::vector<IExpression*>());
            auto thisSetterInvocation = new ClassInstanceMethodInvocation(setter->name, std::vector<IExpression*> {getterInvocation});
            body.emplace_back(new ExpressionStatement(thisSetterInvocation));
        }
        ClassConstructor *constructor = generateNoArgsConstructor(target);
        MethodArgument *arg = new MethodArgument();
        arg->name = argName;
        arg->type = new DataClassDataType(inputClass);
        constructor->methodArguments.emplace_back(arg);
        constructor->methodStatements = body;
        return constructor;
    }
};

#endif
