#include "../domain/DataRepresentation.cpp"

class MethodArgClassRefOptimiser {
private:
    void swapDataClassArgForRef(MethodArgument *methodArgument) {
        if (dynamic_cast<DataClassDataType*>(methodArgument->type) == nullptr) return;
        methodArgument->type = new ReferenceDataType(methodArgument->type);
    }
public:
    void swapDataClassArgsForRefs(ClassConstructor *constructor) {
        for (auto arg : constructor->methodArguments) {
            this->swapDataClassArgForRef(arg);
        }
    }

    void swapDataClassArgsForRefs(ClassMethod *classMethod) {
        for (auto arg : classMethod->methodArguments) {
            this->swapDataClassArgForRef(arg);
        }
    }
    void optimiseDataClass(DataClass *dataClass) {
        for (auto constructor : dataClass->constructors) {
            this->swapDataClassArgsForRefs(constructor);
        }
        for (auto method : dataClass->methods) {
            this->swapDataClassArgsForRefs(method);
        }
    }
};
