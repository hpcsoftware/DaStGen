class
#pragma dastgen class_view
 DataClass {
#pragma dastgen class_view_field ViewOne
    bool a;
#pragma dastgen class_view_field ViewOne ViewTwo
    bool b;
#pragma dastgen class_view_field *
    bool c;
    bool d;
};
