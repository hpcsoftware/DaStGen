#ifndef DASTGEN2_GENERATED__ViewTwoDataClass
#define DASTGEN2_GENERATED__ViewTwoDataClass
#include "./DataClass.h"
#include "./ViewOneDataClass.h"

class DataClass;

class ViewOneDataClass;

class ViewTwoDataClass {

    private: bool b;

    private: bool c;

    public:   ViewTwoDataClass();

    public:   ViewTwoDataClass(bool b, bool c);

    public:   ViewTwoDataClass(ViewTwoDataClass& val);

    public:  bool getB();

    public:  void setB(bool val);

    public:  bool getC();

    public:  void setC(bool val);

    public:  void load(DataClass& val);

    public:  void load(ViewOneDataClass& val);

};

#endif
