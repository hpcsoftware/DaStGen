#include "./ViewTwoDataClass.h"


ViewTwoDataClass::ViewTwoDataClass() {

};



ViewTwoDataClass::ViewTwoDataClass(bool b, bool c) {

    this->setB(b);

    this->setC(c);

};



ViewTwoDataClass::ViewTwoDataClass(ViewTwoDataClass& val) {

    this->b = val.b;

    this->c = val.c;

};



bool ViewTwoDataClass::getB() {

    return this->b;

};



void ViewTwoDataClass::setB(bool val) {

    this->b = val;

};



bool ViewTwoDataClass::getC() {

    return this->c;

};



void ViewTwoDataClass::setC(bool val) {

    this->c = val;

};



void ViewTwoDataClass::load(DataClass& val) {

    this->setB(val.getB());

    this->setC(val.getC());

};



void ViewTwoDataClass::load(ViewOneDataClass& val) {

    this->setB(val.getB());

    this->setC(val.getC());

};

