#ifndef DASTGEN2_GENERATED__ViewOneDataClass
#define DASTGEN2_GENERATED__ViewOneDataClass
#include "./DataClass.h"
#include "./ViewTwoDataClass.h"

class DataClass;

class ViewTwoDataClass;

class ViewOneDataClass {

    private: bool a;

    private: bool b;

    private: bool c;

    public:   ViewOneDataClass();

    public:   ViewOneDataClass(bool a, bool b, bool c);

    public:   ViewOneDataClass(ViewOneDataClass& val);

    public:  bool getA();

    public:  void setA(bool val);

    public:  bool getB();

    public:  void setB(bool val);

    public:  bool getC();

    public:  void setC(bool val);

    public:  void load(DataClass& val);

    public:  void load(ViewTwoDataClass& val);

};

#endif
