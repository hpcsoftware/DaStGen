#ifndef DASTGEN2_GENERATED__DataClass
#define DASTGEN2_GENERATED__DataClass
#include "./ViewOneDataClass.h"
#include "./ViewTwoDataClass.h"

class ViewOneDataClass;

class ViewTwoDataClass;

class DataClass {

    private: bool a;

    private: bool b;

    private: bool c;

    private: bool d;

    public:   DataClass();

    public:   DataClass(bool a, bool b, bool c, bool d);

    public:   DataClass(DataClass& val);

    public:  bool getA();

    public:  void setA(bool val);

    public:  bool getB();

    public:  void setB(bool val);

    public:  bool getC();

    public:  void setC(bool val);

    public:  bool getD();

    public:  void setD(bool val);

    public:  void load(ViewOneDataClass& val);

    public:  void load(ViewTwoDataClass& val);

};

#endif
