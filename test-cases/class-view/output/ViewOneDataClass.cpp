#include "./ViewOneDataClass.h"


ViewOneDataClass::ViewOneDataClass() {

};



ViewOneDataClass::ViewOneDataClass(bool a, bool b, bool c) {

    this->setA(a);

    this->setB(b);

    this->setC(c);

};



ViewOneDataClass::ViewOneDataClass(ViewOneDataClass& val) {

    this->a = val.a;

    this->b = val.b;

    this->c = val.c;

};



bool ViewOneDataClass::getA() {

    return this->a;

};



void ViewOneDataClass::setA(bool val) {

    this->a = val;

};



bool ViewOneDataClass::getB() {

    return this->b;

};



void ViewOneDataClass::setB(bool val) {

    this->b = val;

};



bool ViewOneDataClass::getC() {

    return this->c;

};



void ViewOneDataClass::setC(bool val) {

    this->c = val;

};



void ViewOneDataClass::load(DataClass& val) {

    this->setA(val.getA());

    this->setB(val.getB());

    this->setC(val.getC());

};



void ViewOneDataClass::load(ViewTwoDataClass& val) {

    this->setB(val.getB());

    this->setC(val.getC());

};

