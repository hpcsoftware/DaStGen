#include "./DataClass.h"


DataClass::DataClass() {

};



DataClass::DataClass(bool a, bool b, bool c, bool d) {

    this->setA(a);

    this->setB(b);

    this->setC(c);

    this->setD(d);

};



DataClass::DataClass(DataClass& val) {

    this->a = val.a;

    this->b = val.b;

    this->c = val.c;

    this->d = val.d;

};



bool DataClass::getA() {

    return this->a;

};



void DataClass::setA(bool val) {

    this->a = val;

};



bool DataClass::getB() {

    return this->b;

};



void DataClass::setB(bool val) {

    this->b = val;

};



bool DataClass::getC() {

    return this->c;

};



void DataClass::setC(bool val) {

    this->c = val;

};



bool DataClass::getD() {

    return this->d;

};



void DataClass::setD(bool val) {

    this->d = val;

};



void DataClass::load(ViewOneDataClass& val) {

    this->setA(val.getA());

    this->setB(val.getB());

    this->setC(val.getC());

};



void DataClass::load(ViewTwoDataClass& val) {

    this->setB(val.getB());

    this->setC(val.getC());

};

