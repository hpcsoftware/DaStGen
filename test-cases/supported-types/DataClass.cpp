#include <string>

enum Enum {
    val1 = 1,
    val2 = val1 + 2
};

class
#pragma dastgen nop
DataClass {
public:
    bool boolX;

    char charX;
    signed char charC;
    unsigned char charU;

    double doubleX;
    long double longDoubleX;

    float floatX;

    int intX;
    signed int intS;
    unsigned int intU;

    signed intSS;
    unsigned intUU;

    long longX;
    signed long longS;
    unsigned long longU;

    long int longIntX;
    signed long int longIntS;
    unsigned long int longIntU;

    long long longlongX;
    signed long long longlongS;
    unsigned long long longlongU;

    long long int longlongIntX;
    signed long long int longlongIntS;
    unsigned long long int longlongIntU;

    short shortX;
    signed short shortS;
    unsigned short shortU;

    short int shortIntX;
    signed short int shortIntS;
    unsigned short int shortIntU;

    Enum customEnum;

    bool boolArr[2][2];

    int *intP;
    int &intR;

    std::string str;
};


struct
#pragma dastgen nop
DataStruct {
    bool a;
};


//NOTE DaStGen reads unions like any other aggregate data types (classes, structs, etc.)
//it does not implement the union-specific behaviour
//for this reason, the use of unions is currently discouraged
union
#pragma dastgen nop
DataUnion {
    bool a;
};


class ParentClass {
    bool a;
};

class
#pragma dastgen nop
ChildClass : public ParentClass {
    bool b;
};