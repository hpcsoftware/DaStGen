#include "./DataClass.h"


bool DataClass::getBoolX() {

    return this->boolX;

};



void DataClass::setBoolX(bool val) {

    this->boolX = val;

};



signed char DataClass::getCharX() {

    return this->charX;

};



void DataClass::setCharX(signed char val) {

    this->charX = val;

};



signed char DataClass::getCharC() {

    return this->charC;

};



void DataClass::setCharC(signed char val) {

    this->charC = val;

};



unsigned char DataClass::getCharU() {

    return this->charU;

};



void DataClass::setCharU(unsigned char val) {

    this->charU = val;

};



double DataClass::getDoubleX() {

    return this->doubleX;

};



void DataClass::setDoubleX(double val) {

    this->doubleX = val;

};



long double DataClass::getLongDoubleX() {

    return this->longDoubleX;

};



void DataClass::setLongDoubleX(long double val) {

    this->longDoubleX = val;

};



float DataClass::getFloatX() {

    return this->floatX;

};



void DataClass::setFloatX(float val) {

    this->floatX = val;

};



signed int DataClass::getIntX() {

    return this->intX;

};



void DataClass::setIntX(signed int val) {

    this->intX = val;

};



signed int DataClass::getIntS() {

    return this->intS;

};



void DataClass::setIntS(signed int val) {

    this->intS = val;

};



unsigned int DataClass::getIntU() {

    return this->intU;

};



void DataClass::setIntU(unsigned int val) {

    this->intU = val;

};



signed int DataClass::getIntSS() {

    return this->intSS;

};



void DataClass::setIntSS(signed int val) {

    this->intSS = val;

};



unsigned int DataClass::getIntUU() {

    return this->intUU;

};



void DataClass::setIntUU(unsigned int val) {

    this->intUU = val;

};



signed long DataClass::getLongX() {

    return this->longX;

};



void DataClass::setLongX(signed long val) {

    this->longX = val;

};



signed long DataClass::getLongS() {

    return this->longS;

};



void DataClass::setLongS(signed long val) {

    this->longS = val;

};



unsigned long DataClass::getLongU() {

    return this->longU;

};



void DataClass::setLongU(unsigned long val) {

    this->longU = val;

};



signed long DataClass::getLongIntX() {

    return this->longIntX;

};



void DataClass::setLongIntX(signed long val) {

    this->longIntX = val;

};



signed long DataClass::getLongIntS() {

    return this->longIntS;

};



void DataClass::setLongIntS(signed long val) {

    this->longIntS = val;

};



unsigned long DataClass::getLongIntU() {

    return this->longIntU;

};



void DataClass::setLongIntU(unsigned long val) {

    this->longIntU = val;

};



signed long long DataClass::getLonglongX() {

    return this->longlongX;

};



void DataClass::setLonglongX(signed long long val) {

    this->longlongX = val;

};



signed long long DataClass::getLonglongS() {

    return this->longlongS;

};



void DataClass::setLonglongS(signed long long val) {

    this->longlongS = val;

};



unsigned long long DataClass::getLonglongU() {

    return this->longlongU;

};



void DataClass::setLonglongU(unsigned long long val) {

    this->longlongU = val;

};



signed long long DataClass::getLonglongIntX() {

    return this->longlongIntX;

};



void DataClass::setLonglongIntX(signed long long val) {

    this->longlongIntX = val;

};



signed long long DataClass::getLonglongIntS() {

    return this->longlongIntS;

};



void DataClass::setLonglongIntS(signed long long val) {

    this->longlongIntS = val;

};



unsigned long long DataClass::getLonglongIntU() {

    return this->longlongIntU;

};



void DataClass::setLonglongIntU(unsigned long long val) {

    this->longlongIntU = val;

};



short DataClass::getShortX() {

    return this->shortX;

};



void DataClass::setShortX(short val) {

    this->shortX = val;

};



short DataClass::getShortS() {

    return this->shortS;

};



void DataClass::setShortS(short val) {

    this->shortS = val;

};



unsigned short DataClass::getShortU() {

    return this->shortU;

};



void DataClass::setShortU(unsigned short val) {

    this->shortU = val;

};



short DataClass::getShortIntX() {

    return this->shortIntX;

};



void DataClass::setShortIntX(short val) {

    this->shortIntX = val;

};



short DataClass::getShortIntS() {

    return this->shortIntS;

};



void DataClass::setShortIntS(short val) {

    this->shortIntS = val;

};



unsigned short DataClass::getShortIntU() {

    return this->shortIntU;

};



void DataClass::setShortIntU(unsigned short val) {

    this->shortIntU = val;

};



Enum DataClass::getCustomEnum() {

    return this->customEnum;

};



void DataClass::setCustomEnum(Enum val) {

    this->customEnum = val;

};



bool DataClass::getBoolArr() {

    return this->boolArr;

};



void DataClass::setBoolArr(bool val[2]) {

    this->boolArr = val;

};



signed int* DataClass::getIntP() {

    return this->intP;

};



void DataClass::setIntP(signed int* val) {

    this->intP = val;

};



signed int& DataClass::getIntR() {

    return this->intR;

};



void DataClass::setIntR(signed int& val) {

    this->intR = val;

};



std::string DataClass::getStr() {

    return this->str;

};



void DataClass::setStr(std::string val) {

    this->str = val;

};

