#ifndef DASTGEN2_GENERATED__DataClass
#define DASTGEN2_GENERATED__DataClass
#include "/home/p/Workspaces/c/hpcopt/test-cases/supported-types/DataClass.cpp"

enum Enum;

class DataClass {

    public: bool boolX;

    public: signed char charX;

    public: signed char charC;

    public: unsigned char charU;

    public: double doubleX;

    public: long double longDoubleX;

    public: float floatX;

    public: signed int intX;

    public: signed int intS;

    public: unsigned int intU;

    public: signed int intSS;

    public: unsigned int intUU;

    public: signed long longX;

    public: signed long longS;

    public: unsigned long longU;

    public: signed long longIntX;

    public: signed long longIntS;

    public: unsigned long longIntU;

    public: signed long long longlongX;

    public: signed long long longlongS;

    public: unsigned long long longlongU;

    public: signed long long longlongIntX;

    public: signed long long longlongIntS;

    public: unsigned long long longlongIntU;

    public: short shortX;

    public: short shortS;

    public: unsigned short shortU;

    public: short shortIntX;

    public: short shortIntS;

    public: unsigned short shortIntU;

    public: Enum customEnum;

    public: bool boolArr[2];

    public: signed int* intP;

    public: signed int& intR;

    public: std::string str;

    public:  bool getBoolX();

    public:  void setBoolX(bool val);

    public:  signed char getCharX();

    public:  void setCharX(signed char val);

    public:  signed char getCharC();

    public:  void setCharC(signed char val);

    public:  unsigned char getCharU();

    public:  void setCharU(unsigned char val);

    public:  double getDoubleX();

    public:  void setDoubleX(double val);

    public:  long double getLongDoubleX();

    public:  void setLongDoubleX(long double val);

    public:  float getFloatX();

    public:  void setFloatX(float val);

    public:  signed int getIntX();

    public:  void setIntX(signed int val);

    public:  signed int getIntS();

    public:  void setIntS(signed int val);

    public:  unsigned int getIntU();

    public:  void setIntU(unsigned int val);

    public:  signed int getIntSS();

    public:  void setIntSS(signed int val);

    public:  unsigned int getIntUU();

    public:  void setIntUU(unsigned int val);

    public:  signed long getLongX();

    public:  void setLongX(signed long val);

    public:  signed long getLongS();

    public:  void setLongS(signed long val);

    public:  unsigned long getLongU();

    public:  void setLongU(unsigned long val);

    public:  signed long getLongIntX();

    public:  void setLongIntX(signed long val);

    public:  signed long getLongIntS();

    public:  void setLongIntS(signed long val);

    public:  unsigned long getLongIntU();

    public:  void setLongIntU(unsigned long val);

    public:  signed long long getLonglongX();

    public:  void setLonglongX(signed long long val);

    public:  signed long long getLonglongS();

    public:  void setLonglongS(signed long long val);

    public:  unsigned long long getLonglongU();

    public:  void setLonglongU(unsigned long long val);

    public:  signed long long getLonglongIntX();

    public:  void setLonglongIntX(signed long long val);

    public:  signed long long getLonglongIntS();

    public:  void setLonglongIntS(signed long long val);

    public:  unsigned long long getLonglongIntU();

    public:  void setLonglongIntU(unsigned long long val);

    public:  short getShortX();

    public:  void setShortX(short val);

    public:  short getShortS();

    public:  void setShortS(short val);

    public:  unsigned short getShortU();

    public:  void setShortU(unsigned short val);

    public:  short getShortIntX();

    public:  void setShortIntX(short val);

    public:  short getShortIntS();

    public:  void setShortIntS(short val);

    public:  unsigned short getShortIntU();

    public:  void setShortIntU(unsigned short val);

    public:  Enum getCustomEnum();

    public:  void setCustomEnum(Enum val);

    public:  bool getBoolArr();

    public:  void setBoolArr(bool val[2]);

    public:  signed int* getIntP();

    public:  void setIntP(signed int* val);

    public:  signed int& getIntR();

    public:  void setIntR(signed int& val);

    public:  std::string getStr();

    public:  void setStr(std::string val);

};

#endif
