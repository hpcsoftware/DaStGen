#include "./CompressedSubclass.h"


signed int CompressedSubclass::getI1() {

    return this->i1;

};



void CompressedSubclass::setI1(signed int val) {

    this->i1 = val;

};



bool CompressedSubclass::getA() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 1) >> 0) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedSubclass::setA(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 254) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 0);

};



bool CompressedSubclass::getB() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 2) >> 1) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedSubclass::setB(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 253) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 1);

};



bool CompressedSubclass::getC() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 4) >> 2) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedSubclass::setC(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 251) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 2);

};



bool CompressedSubclass::getD() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 8) >> 3) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedSubclass::setD(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 247) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 3);

};



bool CompressedSubclass::getE() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 16) >> 4) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedSubclass::setE(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 239) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 4);

};



bool CompressedSubclass::getF() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 32) >> 5) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedSubclass::setF(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 223) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 5);

};

