#ifndef DASTGEN2_GENERATED__CompressedSubclass
#define DASTGEN2_GENERATED__CompressedSubclass

class CompressedSubclass {

    private: unsigned char __table[1];

    private: signed int i1;

    public:  signed int getI1();

    public:  void setI1(signed int val);

    public:  bool getA();

    public:  void setA(bool val);

    public:  bool getB();

    public:  void setB(bool val);

    public:  bool getC();

    public:  void setC(bool val);

    public:  bool getD();

    public:  void setD(bool val);

    public:  bool getE();

    public:  void setE(bool val);

    public:  bool getF();

    public:  void setF(bool val);

};

#endif
