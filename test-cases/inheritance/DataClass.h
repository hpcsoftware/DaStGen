//
// Created by p on 06/03/2021.
//

#ifndef HPCOPTIMISER_DATACLASS_H
#define HPCOPTIMISER_DATACLASS_H

class BaseClass {
    bool a, b, c;

    #pragma dastgen ignored
    int i1;
};

class
#pragma dastgen compressed
Subclass : public BaseClass {
    bool d, e, f;
};
#endif //HPCOPTIMISER_DATACLASS_H
