//&>/dev/null;x="${0%.*}";[ ! "$x" -ot "$0" ]||(rm -f "$x";clang -Wno-everything -o "$x" "$0")&&exec "$x" "$@"

#include "stdio.h"
#include "out/generated/Particle.cpp"
#include "out/generated/PositionOfParticle.cpp"
#include "out/generated/VelocityOfParticle.cpp"

int main() {
    Particle p;
    p.setX(1);
    p.setY(2);
    p.setZ(3);

    p.setVx(4);
    p.setVy(5);
    p.setVz(6);

    PositionOfParticle pos;
    pos.load(p);
    printf("Position x = %f , y = %f , z = %f\n\n", pos.getX(), pos.getY(), pos.getZ());

    VelocityOfParticle vel;
    vel.load(p);
    printf("Velocity x = %f , y = %f , z = %f\n\n", vel.getVx(), vel.getVy(), vel.getVz());

    Particle pReconstructed;
    pReconstructed.load(pos);
    pReconstructed.load(vel);
    printf("Reconstructed x = %f , velocity x = %f\n", pReconstructed.getX(), pReconstructed.getVx());
}

