class
#pragma dastgen class_view
Particle {
    #pragma dastgen class_view_field PositionOf
    double x, y, z;

    #pragma dastgen class_view_field VelocityOf
    double vx, vy, vz;
};
