//&>/dev/null;x="${0%.*}";[ ! "$x" -ot "$0" ]||(rm -f "$x";clang -Wno-everything -o "$x" "$0")&&exec "$x" "$@"

#include "stdio.h"
#include "data.cpp"
#include "out/generated/CompressedPoint3DClass.cpp"


int main() {
    CompressedPoint3DClass cp3d;
    printf("Size of original class = %lu bytes , compressed class = %lu bytes\n\n",
           sizeof(Point3DClass), sizeof(CompressedPoint3DClass));

    int x = 1010;
    cp3d.setX(x);
    printf("Setting x = %d (within range) , read back value = %d\n\n", x, cp3d.getX());

    x = 999;
    cp3d.setX(x);
    printf("Setting x = %d (underflow) , read back value = %d\n\n", x, cp3d.getX());

    x = 1256;
    cp3d.setX(x);
    printf("Setting x = %d (overflow), read back value = %d\n", x, cp3d.getX());
}
