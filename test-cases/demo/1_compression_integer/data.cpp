class
#pragma dastgen compressed
#pragma dastgen chunk_size 8
Point3DClass {
public:
    #pragma dastgen int_range 1000 1255
    int x, y, z;
};

struct
#pragma dastgen compressed
#pragma dastgen chunk_size 16
Point3DStruct {
    #pragma dastgen int_range 1000 1255
    int x, y, z;
};

union
#pragma dastgen compressed
#pragma dastgen chunk_size 32
Point3DUnion {
#pragma dastgen int_range 1000 1255
    int x, y, z;
};