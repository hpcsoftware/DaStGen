class
#pragma dastgen compressed
#pragma dastgen chunk_size 16
Point3D {
public:
    #pragma dastgen float_standard float16
    double x, y, z;

    #pragma dastgen float_format mixed_sign 4 8 3 // float8
    double temp;
};
