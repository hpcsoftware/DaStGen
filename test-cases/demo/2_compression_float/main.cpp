//&>/dev/null;x="${0%.*}";[ ! "$x" -ot "$0" ]||(rm -f "$x";clang -Wno-everything -o "$x" "$0")&&exec "$x" "$@"

#include "stdio.h"
#include "data.cpp"
#include "out/generated/CompressedPoint3D.cpp"


int main() {
    CompressedPoint3D cp3d;
    printf("Size of original class = %lu bytes , compressed class = %lu bytes\n\n",
           sizeof(Point3D), sizeof(CompressedPoint3D));

    float temp = 0.125;
    cp3d.setTemp(temp);
    printf("Setting temp = %f (within range, exact) , read back value = %f\n\n", temp, cp3d.getTemp());


    temp = 0.125001;
    cp3d.setTemp(temp);
    printf("Setting temp = %f (within range, truncated) , read back value = %f\n\n", temp, cp3d.getTemp());

    temp = 2e-7;
    cp3d.setTemp(temp);
    printf("Setting temp = %f (outside range, underflow) , read back value = %f\n\n", temp, cp3d.getTemp());

    temp = 1024;
    cp3d.setTemp(temp);
    printf("Setting temp = %f (outside range, overflow) , read back value = %f\n\n", temp, cp3d.getTemp());
}
