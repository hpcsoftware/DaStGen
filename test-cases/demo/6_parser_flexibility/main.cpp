//&>/dev/null;x="${0%.*}";[ ! "$x" -ot "$0" ]||(rm -f "$x";clang -Wno-everything -o "$x" "$0")&&exec "$x" "$@"

#include "stdio.h"

#include "out/generated/StructureOfArraysOfCoordsInt.cpp"
#include "out/generated/StructureOfListsOfCoordsLong.cpp"

int main() {
    StructureOfArraysOfCoordsInt<1> soaInt;
    printf("StructureOfArraysOfCoordsInt type is %s\n\n", sizeof(soaInt.arrOfz) == 4 ? "int" : "long");
    StructureOfArraysOfCoordsLong<1> soaLong;
    printf("StructureOfArraysOfCoordsLong type is %s\n", sizeof(soaLong.arrOfz) == 4 ? "int" : "long");
}
