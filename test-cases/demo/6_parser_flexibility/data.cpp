#ifndef DATA
#define DATA

template<class INT_T>
class Coords {
public:
    INT_T x, y, z;
};

class
#pragma dastgen aosoa
CoordsInt : public Coords<int> {};

class
#pragma dastgen aosoa
CoordsLong : public Coords<long> {};

#endif
