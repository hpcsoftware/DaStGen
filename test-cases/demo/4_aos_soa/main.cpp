//&>/dev/null;x="${0%.*}";[ ! "$x" -ot "$0" ]||(rm -f "$x";clang -Wno-everything -o "$x" "$0")&&exec "$x" "$@"

#include "stdio.h"
#include "cassert"
#include "out/generated/ArrayOfCoords.cpp"
#include "out/generated/StructureOfArraysOfCoords.cpp"

int main() {
    ArrayOfCoords<512> aos;
    for (int i = 0; i < aos.getSize(); i++) {
        aos.arr[i].x = 1;
        aos.arr[i].y = 2;
        aos.arr[i].z = 3;
    }
    StructureOfArraysOfCoords<512> soa;
    soa.loadFromArrayOfCoords(aos);
    for (int i = 0; i < soa.getSize(); i++) {
        assert(soa.arrOfx[i] == 1);
        assert(soa.arrOfy[i] == 2);
        assert(soa.arrOfz[i] == 3);
        soa.arrOfx[i] = 4;
        soa.arrOfy[i] = 5;
        soa.arrOfz[i] = 6;
    }
    printf("AOS -> SOA transfer successful!\n");
    aos.loadFromStructureOfArraysOfCoords(soa);
    for (int i = 0; i < 512; i++) {
        assert(aos.arr[i].x == 4);
        assert(aos.arr[i].y == 5);
        assert(aos.arr[i].z == 6);
    }
    printf("SOA -> AOS transfer successful!\n");
}
