#ifndef DASTGEN2_GENERATED__StructureOfArraysOfDataClass
#define DASTGEN2_GENERATED__StructureOfArraysOfDataClass
#include "./ArrayOfDataClass.h"
#include "./ListOfDataClass.h"
#include "./StructureOfListsOfDataClass.h"
template<signed int size>
class ArrayOfDataClass;

class ListOfDataClass;

class StructureOfListsOfDataClass;
template<signed int size>
class StructureOfArraysOfDataClass {

    public: signed int arrOfx[size];

    public: signed int arrOfy[size];

    public: signed int arrOfz[size];

    public:  signed int getSize();

    public: template<signed int ArrayOfDataClass_size> void loadFromArrayOfDataClass(ArrayOfDataClass<ArrayOfDataClass_size>& other);

    public:  void loadFromListOfDataClass(ListOfDataClass& other);

    public:  void loadFromStructureOfListsOfDataClass(StructureOfListsOfDataClass& other);

};

#endif
