#ifndef DASTGEN2_GENERATED__ArrayOfDataClass
#define DASTGEN2_GENERATED__ArrayOfDataClass
#include "/home/p/Workspaces/c/hpcopt/test-cases/aosoa/DataClass.cpp"
#include "./ListOfDataClass.h"
#include "./StructureOfArraysOfDataClass.h"
#include "./StructureOfListsOfDataClass.h"

class DataClass;

class ListOfDataClass;
template<signed int size>
class StructureOfArraysOfDataClass;

class StructureOfListsOfDataClass;
template<signed int size>
class ArrayOfDataClass {

    public: DataClass arr[size];

    public:  signed int getSize();

    public:  void loadFromListOfDataClass(ListOfDataClass& other);

    public: template<signed int StructureOfArraysOfDataClass_size> void loadFromStructureOfArraysOfDataClass(StructureOfArraysOfDataClass<StructureOfArraysOfDataClass_size>& other);

    public:  void loadFromStructureOfListsOfDataClass(StructureOfListsOfDataClass& other);

};

#endif
