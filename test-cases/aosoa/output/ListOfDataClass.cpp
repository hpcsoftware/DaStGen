#include "./ListOfDataClass.h"


signed int ListOfDataClass::getSize() {

    return this->list.size();

};


template<signed int ArrayOfDataClass_size>
void ListOfDataClass::loadFromArrayOfDataClass(ArrayOfDataClass<ArrayOfDataClass_size>& other) {

    this->list.insert(this->list.begin(), &other.arr[0], &other.arr[other.getSize()]);

};


template<signed int StructureOfArraysOfDataClass_size>
void ListOfDataClass::loadFromStructureOfArraysOfDataClass(StructureOfArraysOfDataClass<StructureOfArraysOfDataClass_size>& other) {

    signed int __loop_counter_0;

    for (__loop_counter_0 = 0; __loop_counter_0 < this->getSize(); __loop_counter_0++) {


        this->list[__loop_counter_0].x = other.x[__loop_counter_0];


        this->list[__loop_counter_0].y = other.y[__loop_counter_0];


        this->list[__loop_counter_0].z = other.z[__loop_counter_0];


    };


};



void ListOfDataClass::loadFromStructureOfListsOfDataClass(StructureOfListsOfDataClass& other) {

    signed int __loop_counter_0;

    for (__loop_counter_0 = 0; __loop_counter_0 < this->getSize(); __loop_counter_0++) {


        this->list[__loop_counter_0].x = other.listOfx[__loop_counter_0];


        this->list[__loop_counter_0].y = other.listOfy[__loop_counter_0];


        this->list[__loop_counter_0].z = other.listOfz[__loop_counter_0];


    };


};

