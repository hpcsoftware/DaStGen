#ifndef DASTGEN2_GENERATED__ListOfDataClass
#define DASTGEN2_GENERATED__ListOfDataClass
#include "vector"
#include "/home/p/Workspaces/c/hpcopt/test-cases/aosoa/DataClass.cpp"
#include "./ArrayOfDataClass.h"
#include "./StructureOfArraysOfDataClass.h"
#include "./StructureOfListsOfDataClass.h"

class DataClass;
template<signed int size>
class ArrayOfDataClass;
template<signed int size>
class StructureOfArraysOfDataClass;

class StructureOfListsOfDataClass;

class ListOfDataClass {

    public: std::vector<DataClass> list;

    public:  signed int getSize();

    public: template<signed int ArrayOfDataClass_size> void loadFromArrayOfDataClass(ArrayOfDataClass<ArrayOfDataClass_size>& other);

    public: template<signed int StructureOfArraysOfDataClass_size> void loadFromStructureOfArraysOfDataClass(StructureOfArraysOfDataClass<StructureOfArraysOfDataClass_size>& other);

    public:  void loadFromStructureOfListsOfDataClass(StructureOfListsOfDataClass& other);

};

#endif
