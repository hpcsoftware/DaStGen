#include "./ArrayOfDataClass.h"
template<signed int size>

signed int ArrayOfDataClass<size>::getSize() {

    return size;

};

template<signed int size>

void ArrayOfDataClass<size>::loadFromListOfDataClass(ListOfDataClass& other) {

    std::copy(other.list.begin(), other.list.end(), this->arr);

};

template<signed int size>
template<signed int StructureOfArraysOfDataClass_size>
void ArrayOfDataClass<size>::loadFromStructureOfArraysOfDataClass(StructureOfArraysOfDataClass<StructureOfArraysOfDataClass_size>& other) {

    signed int __loop_counter_0;

    for (__loop_counter_0 = 0; __loop_counter_0 < this->getSize(); __loop_counter_0++) {


        this->arr[__loop_counter_0].x = other.x[__loop_counter_0];


        this->arr[__loop_counter_0].y = other.y[__loop_counter_0];


        this->arr[__loop_counter_0].z = other.z[__loop_counter_0];


    };


};

template<signed int size>

void ArrayOfDataClass<size>::loadFromStructureOfListsOfDataClass(StructureOfListsOfDataClass& other) {

    signed int __loop_counter_0;

    for (__loop_counter_0 = 0; __loop_counter_0 < this->getSize(); __loop_counter_0++) {


        this->arr[__loop_counter_0].x = other.listOfx[__loop_counter_0];


        this->arr[__loop_counter_0].y = other.listOfy[__loop_counter_0];


        this->arr[__loop_counter_0].z = other.listOfz[__loop_counter_0];


    };


};

