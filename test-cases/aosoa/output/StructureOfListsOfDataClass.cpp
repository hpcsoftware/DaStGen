#include "./StructureOfListsOfDataClass.h"


signed int StructureOfListsOfDataClass::getSize() {

    return this->listOfx.size();

};


template<signed int ArrayOfDataClass_size>
void StructureOfListsOfDataClass::loadFromArrayOfDataClass(ArrayOfDataClass<ArrayOfDataClass_size>& other) {

    signed int __loop_counter_0;

    for (__loop_counter_0 = 0; __loop_counter_0 < this->getSize(); __loop_counter_0++) {


        this->listOfx[__loop_counter_0] = other.arr[__loop_counter_0].x;


        this->listOfy[__loop_counter_0] = other.arr[__loop_counter_0].y;


        this->listOfz[__loop_counter_0] = other.arr[__loop_counter_0].z;


    };


};



void StructureOfListsOfDataClass::loadFromListOfDataClass(ListOfDataClass& other) {

    signed int __loop_counter_0;

    for (__loop_counter_0 = 0; __loop_counter_0 < this->getSize(); __loop_counter_0++) {


        this->listOfx[__loop_counter_0] = other.list[__loop_counter_0].x;


        this->listOfy[__loop_counter_0] = other.list[__loop_counter_0].y;


        this->listOfz[__loop_counter_0] = other.list[__loop_counter_0].z;


    };


};


template<signed int StructureOfArraysOfDataClass_size>
void StructureOfListsOfDataClass::loadFromStructureOfArraysOfDataClass(StructureOfArraysOfDataClass<StructureOfArraysOfDataClass_size>& other) {

    this->listOfx.insert(this->listOfx.begin(), &other.arrOfx[0], &other.arrOfx[other.getSize()]);

    this->listOfy.insert(this->listOfy.begin(), &other.arrOfy[0], &other.arrOfy[other.getSize()]);

    this->listOfz.insert(this->listOfz.begin(), &other.arrOfz[0], &other.arrOfz[other.getSize()]);

};

