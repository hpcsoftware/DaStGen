#include "./StructureOfArraysOfDataClass.h"
template<signed int size>

signed int StructureOfArraysOfDataClass<size>::getSize() {

    return size;

};

template<signed int size>
template<signed int ArrayOfDataClass_size>
void StructureOfArraysOfDataClass<size>::loadFromArrayOfDataClass(ArrayOfDataClass<ArrayOfDataClass_size>& other) {

    signed int __loop_counter_0;

    for (__loop_counter_0 = 0; __loop_counter_0 < this->getSize(); __loop_counter_0++) {


        this->arrOfx[__loop_counter_0] = other.arr[__loop_counter_0].x;


        this->arrOfy[__loop_counter_0] = other.arr[__loop_counter_0].y;


        this->arrOfz[__loop_counter_0] = other.arr[__loop_counter_0].z;


    };


};

template<signed int size>

void StructureOfArraysOfDataClass<size>::loadFromListOfDataClass(ListOfDataClass& other) {

    signed int __loop_counter_0;

    for (__loop_counter_0 = 0; __loop_counter_0 < this->getSize(); __loop_counter_0++) {


        this->arrOfx[__loop_counter_0] = other.list[__loop_counter_0].x;


        this->arrOfy[__loop_counter_0] = other.list[__loop_counter_0].y;


        this->arrOfz[__loop_counter_0] = other.list[__loop_counter_0].z;


    };


};

template<signed int size>

void StructureOfArraysOfDataClass<size>::loadFromStructureOfListsOfDataClass(StructureOfListsOfDataClass& other) {

    std::copy(other.listOfx.begin(), other.listOfx.end(), this->arrOfx);

    std::copy(other.listOfy.begin(), other.listOfy.end(), this->arrOfy);

    std::copy(other.listOfz.begin(), other.listOfz.end(), this->arrOfz);

};

