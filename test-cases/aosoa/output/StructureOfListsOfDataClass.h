#ifndef DASTGEN2_GENERATED__StructureOfListsOfDataClass
#define DASTGEN2_GENERATED__StructureOfListsOfDataClass
#include "vector"
#include "./ArrayOfDataClass.h"
#include "./ListOfDataClass.h"
#include "./StructureOfArraysOfDataClass.h"
template<signed int size>
class ArrayOfDataClass;

class ListOfDataClass;
template<signed int size>
class StructureOfArraysOfDataClass;

class StructureOfListsOfDataClass {

    public: std::vector<signed int> listOfx;

    public: std::vector<signed int> listOfy;

    public: std::vector<signed int> listOfz;

    public:  signed int getSize();

    public: template<signed int ArrayOfDataClass_size> void loadFromArrayOfDataClass(ArrayOfDataClass<ArrayOfDataClass_size>& other);

    public:  void loadFromListOfDataClass(ListOfDataClass& other);

    public: template<signed int StructureOfArraysOfDataClass_size> void loadFromStructureOfArraysOfDataClass(StructureOfArraysOfDataClass<StructureOfArraysOfDataClass_size>& other);

};

#endif
