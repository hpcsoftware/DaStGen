#ifndef DASTGEN2_GENERATED__CompressedChildDataClass
#define DASTGEN2_GENERATED__CompressedChildDataClass

class CompressedChildDataClass {

    private: unsigned char __table[1];

    public:  bool getA();

    public:  void setA(bool val);

    public:  bool getB();

    public:  void setB(bool val);

    public:  bool getC();

    public:  void setC(bool val);

};

#endif
