#include "./CompressedChildDataClass.h"


bool CompressedChildDataClass::getA() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 1) >> 0) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedChildDataClass::setA(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 254) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 0);

};



bool CompressedChildDataClass::getB() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 2) >> 1) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedChildDataClass::setB(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 253) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 1);

};



bool CompressedChildDataClass::getC() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 4) >> 2) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedChildDataClass::setC(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 251) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 2);

};

