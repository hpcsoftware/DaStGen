#ifndef DASTGEN2_GENERATED__CompressedParentDataClass
#define DASTGEN2_GENERATED__CompressedParentDataClass
#include "./CompressedChildDataClass.h"

class CompressedChildDataClass;

class CompressedParentDataClass {

    private: unsigned char __table[0];

    private: signed int d;

    private: signed int e;

    private: signed int f;

    private: CompressedChildDataClass child;

    public:  signed int getD();

    public:  void setD(signed int val);

    public:  signed int getE();

    public:  void setE(signed int val);

    public:  signed int getF();

    public:  void setF(signed int val);

    public:  CompressedChildDataClass getChild();

    public:  void setChild(CompressedChildDataClass& val);

};

#endif
