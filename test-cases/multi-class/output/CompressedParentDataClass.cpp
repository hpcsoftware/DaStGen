#include "./CompressedParentDataClass.h"


signed int CompressedParentDataClass::getD() {

    return this->d;

};



void CompressedParentDataClass::setD(signed int val) {

    this->d = val;

};



signed int CompressedParentDataClass::getE() {

    return this->e;

};



void CompressedParentDataClass::setE(signed int val) {

    this->e = val;

};



signed int CompressedParentDataClass::getF() {

    return this->f;

};



void CompressedParentDataClass::setF(signed int val) {

    this->f = val;

};



CompressedChildDataClass CompressedParentDataClass::getChild() {

    return this->child;

};



void CompressedParentDataClass::setChild(CompressedChildDataClass& val) {

    this->child = val;

};

