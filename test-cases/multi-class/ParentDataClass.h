#ifndef HPCOPTIMISER_PARENTDATACLASS_H
#define HPCOPTIMISER_PARENTDATACLASS_H

#include "ChildDataClass.h"

class
#pragma dastgen compressed
#pragma dastgen substituted
ParentDataClass {
    int d, e, f;

    #pragma dastgen substitute_with CompressedChildDataClass
    ChildDataClass child;
};

#endif //HPCOPTIMISER_PARENTDATACLASS_H
