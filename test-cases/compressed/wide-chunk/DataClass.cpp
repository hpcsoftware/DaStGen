class
#pragma dastgen compressed
#pragma dastgen chunk_size 32
DataClass {
    #pragma dastgen float_standard float16
    float f1, f2;
};