#include "./CompressedDataClass.h"


float CompressedDataClass::getF1() {

    unsigned int __fetched;

    __fetched = (((this->__table[0] & 65535) >> 0) << 0);

    unsigned int __decompressed;

    __decompressed = ((__fetched << 16) & static_cast<unsigned long>(2147483648)) | ((static_cast<unsigned long>((__fetched >> 10) & 31) + 112) << 23) | ((__fetched << 13) & static_cast<unsigned long>(8388607));

    return reinterpret_cast<float&>(__decompressed);

};



void CompressedDataClass::setF1(float val) {

    unsigned short __compressed;

    __compressed = ((reinterpret_cast<unsigned int&>(val) >> 16) & static_cast<unsigned long>(32768)) | ((static_cast<unsigned long>((reinterpret_cast<unsigned int&>(val) >> 23) & 255) + -112) << 10) | ((reinterpret_cast<unsigned int&>(val) >> 13) & static_cast<unsigned long>(1023));

    this->__table[0] = ((this->__table[0] & 4294901760) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 0);

};



float CompressedDataClass::getF2() {

    unsigned int __fetched;

    __fetched = (((this->__table[0] & 4294901760) >> 16) << 0);

    unsigned int __decompressed;

    __decompressed = ((__fetched << 16) & static_cast<unsigned long>(2147483648)) | ((static_cast<unsigned long>((__fetched >> 10) & 31) + 112) << 23) | ((__fetched << 13) & static_cast<unsigned long>(8388607));

    return reinterpret_cast<float&>(__decompressed);

};



void CompressedDataClass::setF2(float val) {

    unsigned short __compressed;

    __compressed = ((reinterpret_cast<unsigned int&>(val) >> 16) & static_cast<unsigned long>(32768)) | ((static_cast<unsigned long>((reinterpret_cast<unsigned int&>(val) >> 23) & 255) + -112) << 10) | ((reinterpret_cast<unsigned int&>(val) >> 13) & static_cast<unsigned long>(1023));

    this->__table[0] = ((this->__table[0] & 65535) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 16);

};

