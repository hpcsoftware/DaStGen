#ifndef DASTGEN2_GENERATED__CompressedDataClass
#define DASTGEN2_GENERATED__CompressedDataClass

class CompressedDataClass {

    private: unsigned int __table[1];

    public:  float getF1();

    public:  void setF1(float val);

    public:  float getF2();

    public:  void setF2(float val);

};

#endif
