#include "./CompressedDataClass.h"


bool CompressedDataClass::getIgnored() {

    return this->ignored;

};



void CompressedDataClass::setIgnored(bool val) {

    this->ignored = val;

};



bool CompressedDataClass::getA() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 1) >> 0) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedDataClass::setA(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 254) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 0);

};



signed int CompressedDataClass::getI1() {

    unsigned int __fetched;

    __fetched = (((this->__table[0] & 30) >> 1) << 0);

    unsigned int __decompressed;

    __decompressed = static_cast<signed int>(__fetched + 0);

    return reinterpret_cast<signed int&>(__decompressed);

};



void CompressedDataClass::setI1(signed int val) {

    unsigned char __compressed;

    __compressed = val - 0;

    this->__table[0] = ((this->__table[0] & 225) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 1);

};



signed char CompressedDataClass::getI2() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 224) >> 5) << 0) | (((this->__table[1] & 1) >> 0) << 3);

    unsigned char __decompressed;

    __decompressed = static_cast<signed char>(__fetched + -10);

    return reinterpret_cast<signed char&>(__decompressed);

};



void CompressedDataClass::setI2(signed char val) {

    unsigned char __compressed;

    __compressed = val - -10;

    this->__table[0] = ((this->__table[0] & 31) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 5);

    this->__table[1] = ((this->__table[1] & 254) | (reinterpret_cast<unsigned int&>(__compressed) >> 3) << 0);

};



float CompressedDataClass::getF1() {

    unsigned int __fetched;

    __fetched = (((this->__table[1] & 254) >> 1) << 0) | (((this->__table[2] & 255) >> 0) << 7) | (((this->__table[3] & 1) >> 0) << 15);

    unsigned int __decompressed;

    __decompressed = ((__fetched << 16) & static_cast<unsigned long>(2147483648)) | ((static_cast<unsigned long>((__fetched >> 10) & 31) + 112) << 23) | ((__fetched << 13) & static_cast<unsigned long>(8388607));

    return reinterpret_cast<float&>(__decompressed);

};



void CompressedDataClass::setF1(float val) {

    unsigned short __compressed;

    __compressed = ((reinterpret_cast<unsigned int&>(val) >> 16) & static_cast<unsigned long>(32768)) | ((static_cast<unsigned long>((reinterpret_cast<unsigned int&>(val) >> 23) & 255) + -112) << 10) | ((reinterpret_cast<unsigned int&>(val) >> 13) & static_cast<unsigned long>(1023));

    this->__table[1] = ((this->__table[1] & 1) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 1);

    this->__table[2] = ((this->__table[2] & 0) | (reinterpret_cast<unsigned int&>(__compressed) >> 7) << 0);

    this->__table[3] = ((this->__table[3] & 254) | (reinterpret_cast<unsigned int&>(__compressed) >> 15) << 0);

};



float CompressedDataClass::getF2() {

    unsigned int __fetched;

    __fetched = (((this->__table[3] & 254) >> 1) << 0) | (((this->__table[4] & 255) >> 0) << 7) | (((this->__table[5] & 1) >> 0) << 15);

    unsigned int __decompressed;

    __decompressed = ((__fetched << 16) & static_cast<unsigned long>(2147483648)) | ((static_cast<unsigned long>((__fetched >> 10) & 31) + 112) << 23) | ((__fetched << 13) & static_cast<unsigned long>(8388607));

    return reinterpret_cast<float&>(__decompressed);

};



void CompressedDataClass::setF2(float val) {

    unsigned short __compressed;

    __compressed = ((reinterpret_cast<unsigned int&>(val) >> 16) & static_cast<unsigned long>(32768)) | ((static_cast<unsigned long>((reinterpret_cast<unsigned int&>(val) >> 23) & 255) + -112) << 10) | ((reinterpret_cast<unsigned int&>(val) >> 13) & static_cast<unsigned long>(1023));

    this->__table[3] = ((this->__table[3] & 1) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 1);

    this->__table[4] = ((this->__table[4] & 0) | (reinterpret_cast<unsigned int&>(__compressed) >> 7) << 0);

    this->__table[5] = ((this->__table[5] & 254) | (reinterpret_cast<unsigned int&>(__compressed) >> 15) << 0);

};



Enum CompressedDataClass::getE() {

    unsigned int __fetched;

    __fetched = (((this->__table[5] & 2) >> 1) << 0);

    unsigned int __decompressed;

    __decompressed = static_cast<Enum>(__fetched == 1 ? 3 : __fetched == 0 ? 1 : 0);

    return reinterpret_cast<Enum&>(__decompressed);

};



void CompressedDataClass::setE(Enum val) {

    unsigned char __compressed;

    __compressed = static_cast<Enum>(val == 3 ? 1 : val == 1 ? 0 : 0);

    this->__table[5] = ((this->__table[5] & 253) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 1);

};

