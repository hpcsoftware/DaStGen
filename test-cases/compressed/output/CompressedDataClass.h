#ifndef DASTGEN2_GENERATED__CompressedDataClass
#define DASTGEN2_GENERATED__CompressedDataClass
#include "/home/p/Workspaces/c/hpcopt/test-cases/compressed/DataClass.cpp"

enum Enum;

class CompressedDataClass {

    private: unsigned char __table[6];

    private: bool ignored;

    public:  bool getIgnored();

    public:  void setIgnored(bool val);

    public:  bool getA();

    public:  void setA(bool val);

    public:  signed int getI1();

    public:  void setI1(signed int val);

    public:  signed char getI2();

    public:  void setI2(signed char val);

    public:  float getF1();

    public:  void setF1(float val);

    public:  float getF2();

    public:  void setF2(float val);

    public:  Enum getE();

    public:  void setE(Enum val);

};

#endif
