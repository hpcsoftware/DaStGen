#include <string>

enum Enum {
    val1 = 1,
    val2 = val1 + 2
};

#pragma dastgen compressed
class DataClass {
public:
    bool a, b, c, d, e, f, g;

    #pragma dastgen ignored
    bool ignored;

    #pragma dastgen int_range 0 10
    int i1;

    #pragma dastgen int_range -10 0
    char i2;

    #pragma dastgen float_standard float16
    float f1;

    #pragma dastgen float_format mixed_sign 5 15 10
    float f2;

    Enum en;
};
