class
#pragma dastgen class_view
#pragma dastgen forward BooleansOnlyDataClass compressed
#pragma dastgen forward BooleansOnlyDataClass::b3 ignored
DataClass {
    #pragma dastgen class_view_field BooleansOnly
    bool b1, b2, b3;

    #pragma dastgen class_view_field IntegersOnly
    int i1, i2, i3;
};
