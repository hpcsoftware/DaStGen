#ifndef DASTGEN2_GENERATED__BooleansOnlyDataClass
#define DASTGEN2_GENERATED__BooleansOnlyDataClass
#include "./DataClass.h"
#include "./IntegersOnlyDataClass.h"

class DataClass;

class IntegersOnlyDataClass;

class BooleansOnlyDataClass {

    private: bool b1;

    private: bool b2;

    private: bool b3;

    public:   BooleansOnlyDataClass();

    public:   BooleansOnlyDataClass(bool b1, bool b2, bool b3);

    public:   BooleansOnlyDataClass(BooleansOnlyDataClass& val);

    public:  bool getB1();

    public:  void setB1(bool val);

    public:  bool getB2();

    public:  void setB2(bool val);

    public:  bool getB3();

    public:  void setB3(bool val);

    public:  void load(DataClass& val);

    public:  void load(IntegersOnlyDataClass& val);

};

#endif
