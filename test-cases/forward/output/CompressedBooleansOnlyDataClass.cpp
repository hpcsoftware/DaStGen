#include "./CompressedBooleansOnlyDataClass.h"


bool CompressedBooleansOnlyDataClass::getB3() {

    return this->b3;

};



void CompressedBooleansOnlyDataClass::setB3(bool val) {

    this->b3 = val;

};



bool CompressedBooleansOnlyDataClass::getB1() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 1) >> 0) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedBooleansOnlyDataClass::setB1(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 254) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 0);

};



bool CompressedBooleansOnlyDataClass::getB2() {

    unsigned char __fetched;

    __fetched = (((this->__table[0] & 2) >> 1) << 0);

    unsigned char __decompressed;

    __decompressed = static_cast<bool>(__fetched);

    return reinterpret_cast<bool&>(__decompressed);

};



void CompressedBooleansOnlyDataClass::setB2(bool val) {

    unsigned char __compressed;

    __compressed = val;

    this->__table[0] = ((this->__table[0] & 253) | (reinterpret_cast<unsigned int&>(__compressed) >> 0) << 1);

};

