#ifndef DASTGEN2_GENERATED__DataClass
#define DASTGEN2_GENERATED__DataClass
#include "./BooleansOnlyDataClass.h"
#include "./IntegersOnlyDataClass.h"

class BooleansOnlyDataClass;

class IntegersOnlyDataClass;

class DataClass {

    private: bool b1;

    private: bool b2;

    private: bool b3;

    private: signed int i1;

    private: signed int i2;

    private: signed int i3;

    public:   DataClass();

    public:   DataClass(bool b1, bool b2, bool b3, signed int i1, signed int i2, signed int i3);

    public:   DataClass(DataClass& val);

    public:  bool getB1();

    public:  void setB1(bool val);

    public:  bool getB2();

    public:  void setB2(bool val);

    public:  bool getB3();

    public:  void setB3(bool val);

    public:  signed int getI1();

    public:  void setI1(signed int val);

    public:  signed int getI2();

    public:  void setI2(signed int val);

    public:  signed int getI3();

    public:  void setI3(signed int val);

    public:  void load(BooleansOnlyDataClass& val);

    public:  void load(IntegersOnlyDataClass& val);

};

#endif
