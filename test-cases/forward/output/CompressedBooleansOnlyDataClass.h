#ifndef DASTGEN2_GENERATED__CompressedBooleansOnlyDataClass
#define DASTGEN2_GENERATED__CompressedBooleansOnlyDataClass

class CompressedBooleansOnlyDataClass {

    private: unsigned char __table[1];

    private: bool b3;

    public:  bool getB3();

    public:  void setB3(bool val);

    public:  bool getB1();

    public:  void setB1(bool val);

    public:  bool getB2();

    public:  void setB2(bool val);

};

#endif
