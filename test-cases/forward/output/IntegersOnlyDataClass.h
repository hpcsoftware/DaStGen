#ifndef DASTGEN2_GENERATED__IntegersOnlyDataClass
#define DASTGEN2_GENERATED__IntegersOnlyDataClass
#include "./DataClass.h"
#include "./BooleansOnlyDataClass.h"

class DataClass;

class BooleansOnlyDataClass;

class IntegersOnlyDataClass {

    private: signed int i1;

    private: signed int i2;

    private: signed int i3;

    public:   IntegersOnlyDataClass();

    public:   IntegersOnlyDataClass(signed int i1, signed int i2, signed int i3);

    public:   IntegersOnlyDataClass(IntegersOnlyDataClass& val);

    public:  signed int getI1();

    public:  void setI1(signed int val);

    public:  signed int getI2();

    public:  void setI2(signed int val);

    public:  signed int getI3();

    public:  void setI3(signed int val);

    public:  void load(DataClass& val);

    public:  void load(BooleansOnlyDataClass& val);

};

#endif
