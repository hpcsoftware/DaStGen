#include "./IntegersOnlyDataClass.h"


IntegersOnlyDataClass::IntegersOnlyDataClass() {

};



IntegersOnlyDataClass::IntegersOnlyDataClass(signed int i1, signed int i2, signed int i3) {

    this->setI1(i1);

    this->setI2(i2);

    this->setI3(i3);

};



IntegersOnlyDataClass::IntegersOnlyDataClass(IntegersOnlyDataClass& val) {

    this->i1 = val.i1;

    this->i2 = val.i2;

    this->i3 = val.i3;

};



signed int IntegersOnlyDataClass::getI1() {

    return this->i1;

};



void IntegersOnlyDataClass::setI1(signed int val) {

    this->i1 = val;

};



signed int IntegersOnlyDataClass::getI2() {

    return this->i2;

};



void IntegersOnlyDataClass::setI2(signed int val) {

    this->i2 = val;

};



signed int IntegersOnlyDataClass::getI3() {

    return this->i3;

};



void IntegersOnlyDataClass::setI3(signed int val) {

    this->i3 = val;

};



void IntegersOnlyDataClass::load(DataClass& val) {

    this->setI1(val.getI1());

    this->setI2(val.getI2());

    this->setI3(val.getI3());

};



void IntegersOnlyDataClass::load(BooleansOnlyDataClass& val) {

};

