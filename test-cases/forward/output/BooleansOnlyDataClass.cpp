#include "./BooleansOnlyDataClass.h"


BooleansOnlyDataClass::BooleansOnlyDataClass() {

};



BooleansOnlyDataClass::BooleansOnlyDataClass(bool b1, bool b2, bool b3) {

    this->setB1(b1);

    this->setB2(b2);

    this->setB3(b3);

};



BooleansOnlyDataClass::BooleansOnlyDataClass(BooleansOnlyDataClass& val) {

    this->b1 = val.b1;

    this->b2 = val.b2;

    this->b3 = val.b3;

};



bool BooleansOnlyDataClass::getB1() {

    return this->b1;

};



void BooleansOnlyDataClass::setB1(bool val) {

    this->b1 = val;

};



bool BooleansOnlyDataClass::getB2() {

    return this->b2;

};



void BooleansOnlyDataClass::setB2(bool val) {

    this->b2 = val;

};



bool BooleansOnlyDataClass::getB3() {

    return this->b3;

};



void BooleansOnlyDataClass::setB3(bool val) {

    this->b3 = val;

};



void BooleansOnlyDataClass::load(DataClass& val) {

    this->setB1(val.getB1());

    this->setB2(val.getB2());

    this->setB3(val.getB3());

};



void BooleansOnlyDataClass::load(IntegersOnlyDataClass& val) {

};

