#include "./DataClass.h"


DataClass::DataClass() {

};



DataClass::DataClass(bool b1, bool b2, bool b3, signed int i1, signed int i2, signed int i3) {

    this->setB1(b1);

    this->setB2(b2);

    this->setB3(b3);

    this->setI1(i1);

    this->setI2(i2);

    this->setI3(i3);

};



DataClass::DataClass(DataClass& val) {

    this->b1 = val.b1;

    this->b2 = val.b2;

    this->b3 = val.b3;

    this->i1 = val.i1;

    this->i2 = val.i2;

    this->i3 = val.i3;

};



bool DataClass::getB1() {

    return this->b1;

};



void DataClass::setB1(bool val) {

    this->b1 = val;

};



bool DataClass::getB2() {

    return this->b2;

};



void DataClass::setB2(bool val) {

    this->b2 = val;

};



bool DataClass::getB3() {

    return this->b3;

};



void DataClass::setB3(bool val) {

    this->b3 = val;

};



signed int DataClass::getI1() {

    return this->i1;

};



void DataClass::setI1(signed int val) {

    this->i1 = val;

};



signed int DataClass::getI2() {

    return this->i2;

};



void DataClass::setI2(signed int val) {

    this->i2 = val;

};



signed int DataClass::getI3() {

    return this->i3;

};



void DataClass::setI3(signed int val) {

    this->i3 = val;

};



void DataClass::load(BooleansOnlyDataClass& val) {

    this->setB1(val.getB1());

    this->setB2(val.getB2());

    this->setB3(val.getB3());

};



void DataClass::load(IntegersOnlyDataClass& val) {

    this->setI1(val.getI1());

    this->setI2(val.getI2());

    this->setI3(val.getI3());

};

