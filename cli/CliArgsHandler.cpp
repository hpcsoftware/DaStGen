#include <getopt.h>
#include <string>
#include <iostream>

class CliArgsHandler {
private:
    std::string outputDir = "./out/generated";
    int nonOptsArgsIndex = 0;
    char **nonOptsArgsPointer = nullptr;

public:
    void parse(int argc, char** argv) {
        int c;
        opterr = 0;
        if (argc == 1) {
            std::cout << this->displayUsage();
            exit(0);
        }
        while ((c = getopt (argc, argv, "ho:")) != -1) {
            switch (c) {
                case 'o':
                    this->outputDir = optarg;
                    break;
                case 'h':
                    std::cout << this->displayUsage();
                    exit(0);
                default:
                    std::cerr << "Unknown option" << std::endl << std::endl;
                    std::cout << this->displayUsage();
                    exit(1);
            }
        }
        this->nonOptsArgsIndex = argc - optind;
        this->nonOptsArgsPointer = argv + optind;
    }

    std::string displayUsage() {
        return
               "Usage:\n"
               "\n"
               "dastgen2 [option...] filepath [filepath...]\n"
               "\n"
               "Options:\n"
               "\n"
               "-o  sets the output folder\n"
               "-h  displays this message\n";
    }

    std::string getOutputPath() {
        return this->outputDir;
    }

    int argc() {
        return this->nonOptsArgsIndex;
    }

    char **argv() {
        return this->nonOptsArgsPointer;
    }

};
