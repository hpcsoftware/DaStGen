#ifndef __DASTGEN2_SourceParser__
#define __DASTGEN2_SourceParser__

class SourceParser {
    virtual std::vector<DataClass *> parseSourceFiles(int argc, char** filenames) {
        return std::vector<DataClass *>();
    }
};

#endif