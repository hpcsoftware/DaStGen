#include <iostream>
#include <map>
#include "../domain/DataRepresentation.cpp"

#ifndef __DASTGEN2_PrettyPrinter__
#define __DASTGEN2_PrettyPrinter__

class PrettyPrinter {
private:
    const std::map<PrimitiveDataType, std::string> typeMap = std::map<PrimitiveDataType, std::string> {
            {PrimitiveDataType::UNKNOWN,        "unknown"},
            {PrimitiveDataType::SHORT,          "short"},
            {PrimitiveDataType::UNSIGNED_SHORT, "unsigned short"},
            {PrimitiveDataType::CHAR,           "char"},
            {PrimitiveDataType::UNSIGNED_CHAR,  "unsigned char"},
            {PrimitiveDataType::BOOLEAN,        "bool"},
            {PrimitiveDataType::INT,            "int"}
    };
private:
    std::string dataTypeToString(IDataType *dataType) {
        if (dynamic_cast<PrimitiveData*>(dataType) != nullptr) {
            PrimitiveData *data = reinterpret_cast<PrimitiveData*>(dataType);
            return typeMap.at(data->getDataType());
        }
        if (dynamic_cast<ConstSizeArrayDataType*>(dataType) != nullptr) {
            ConstSizeArrayDataType* constSizeArrayDataType = reinterpret_cast<ConstSizeArrayDataType*>(dataType);
            return dataTypeToString(constSizeArrayDataType) + "[" + constSizeArrayDataType->getItemCount() + "]";
        }
        std::cerr << "Unknown data type" << std::endl;
        return "unknown data type";
    }
public:
    void print(DataClass *dataClass) {
        for (auto annotation : dataClass->annotations) {
            std::string annotationTokens;
            for (auto token : annotation->tokens) {
                annotationTokens += token + " ";
            }
            std::cout << "@(" << annotationTokens << ") ";
        }
        std::cout << std::endl;
        std::string namespacesString;
        for (auto ns: dataClass->namespaces) {
            namespacesString += ns + "::";
        }
        if (!namespacesString.empty()) {
            namespacesString.pop_back(); //removing the dangling "::"
            namespacesString.pop_back();
        }
        std::cout << "ns(" << namespacesString << ") " << dataClass->name << " {" << std::endl;
        for (auto field : dataClass->fields) {
            std::cout << "  ";
            for (auto annotation : field->annotations) {
                std::string annotationTokens;
                for (auto token : annotation->tokens) {
                    annotationTokens += token + " ";
                }
                std::cout << "@(" << annotationTokens << ") ";
            }
            std::cout << this->dataTypeToString(field->type) << " " << field->name << std::endl;
        }
        std::cout << "}" << std::endl << std::endl;
    }
};

#endif