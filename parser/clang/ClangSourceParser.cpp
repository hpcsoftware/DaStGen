#include <vector>
#include "PragmaHandler.cpp"
#include "CompilerWrapper.cpp"
#include "ClangAstConsumer.cpp"
#include "../../domain/DataRepresentation.cpp"
#include "../SourceParser.cpp"

class ClangSourceParser : public SourceParser {
private:
    const std::string dastgenPragmaPrefix = "dastgen";
public:
    std::vector<DataClass *> parseSourceFiles(int filesCount, char** filenames) override {
        std::vector<DataClass*> *classes = new std::vector<DataClass *>();
        std::vector<Enum *> *enums = new std::vector<Enum *>();
        CompilerWrapper *wrapper = new CompilerWrapper();
        ClangAstConsumer *clangAstConsumer = new ClangAstConsumer(classes, enums);
        wrapper
                ->addPragmaHandler(new AnnotationAdder(dastgenPragmaPrefix, wrapper->getRewriter()))
                ->setASTConsumer(clangAstConsumer);
        for (int i = 0; i < filesCount; i++) {
            wrapper->parseFile(std::string(filenames[i]));
        }
        return *classes;
    }

};
