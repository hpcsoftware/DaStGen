#include <utility>
#include <vector>
#include <clang/Tooling/Tooling.h>
#include "PragmaHandler.cpp"
#include "CompilerWrapper.cpp"
#include "ClangAstConsumer.cpp"
#include "../../domain/DataRepresentation.cpp"
#include "../SourceParser.cpp"

class ConsumeASTActionV3 : public clang::ASTFrontendAction {
public:
    explicit ConsumeASTActionV3(std::string filename,
                              std::vector<DataClass*> *dataClasses,
                              std::vector<Enum *> *enums) :
            _filename(std::move(filename)),
            _dataClasses(dataClasses),
            _enums(enums) {}
protected:
    std::string _filename;
    std::vector<DataClass *> *_dataClasses;
    std::vector<Enum *> *_enums;

    bool BeginSourceFileAction(clang::CompilerInstance &CI) override {
        auto fileOrError = CI.getFileManager().getFile(_filename);
        if (!fileOrError) {
            llvm::errs() << "File not found: " << fileOrError.getError().message();
            return false;
        }
        CI.getPreprocessor().AddPragmaHandler(new TokenLevelPragmaHandler("dastgen"));
        const clang::FileEntry *file = fileOrError.get();
        clang::FileID mainFileID = CI.getSourceManager()
                .createFileID(file, clang::SourceLocation(), clang::SrcMgr::C_User);
        CI.getSourceManager().setMainFileID(mainFileID);
        return true;
    }

    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
            clang::CompilerInstance &Compiler, llvm::StringRef InFile) {
        return std::make_unique<ClangAstConsumer>(_dataClasses, _enums);
    }
};

class ClangSourceParserV3 : public SourceParser {
private:
    const std::string dastgenPragmaPrefix = "dastgen";

    void removeDuplicateClasses(std::vector<DataClass *> *classes) {
        std::vector<DataClass *> uniqueClasses;
        std::set<std::string> names;
        for (auto dataClass : *classes) {
            std::string name;
            for (const auto& ns : dataClass->namespaces) name += ns;
            name += dataClass->name;
            if (names.count(name) == 1) continue;
            uniqueClasses.emplace_back(dataClass);
            names.insert(name);
        }
        classes->clear();
        classes->insert(classes->end(), uniqueClasses.begin(), uniqueClasses.end());
    }

public:
    std::vector<DataClass *> parseSourceFiles(int filesCount, char** filenames) override {
        std::vector<DataClass *> *dataClasses = new std::vector<DataClass *>();
        std::vector<Enum*> *enums = new std::vector<Enum*>();

        for (int i = 0; i < filesCount; i++) {
            clang::tooling::runToolOnCode(std::make_unique<ConsumeASTActionV3>(std::string(filenames[i]), dataClasses, enums), "");
        }

        this->removeDuplicateClasses(dataClasses);
        return *dataClasses;
    }

};
