#include <string>
#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Lex/HeaderSearch.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include <clang/Parse/ParseAST.h>
#include <clang/Frontend/ASTConsumers.h>
#include <clang/Lex/PreprocessorOptions.h>
#include <iostream>

class CompilerWrapper {
    
private:
    clang::Rewriter *rewriter;
    clang::CompilerInstance *compilerInstance;
    clang::ASTConsumer *astConsumer;
    
    clang::CompilerInstance* getCompilerInstance() {
        clang::CompilerInstance *ci = new clang::CompilerInstance();
        ci->createDiagnostics();
        std::shared_ptr<clang::TargetOptions> PTO = std::make_shared<clang::TargetOptions>();
        PTO->Triple = llvm::sys::getDefaultTargetTriple();
        clang::TargetInfo *PTI = clang::TargetInfo::CreateTargetInfo(ci->getDiagnostics(), PTO);
        ci->setTarget(PTI);
        ci->getLangOpts().GNUMode = 1;
        ci->getLangOpts().GNUKeywords = 1;
        ci->getLangOpts().CXXExceptions = 1;
        ci->getLangOpts().RTTI = 1;
        ci->getLangOpts().Bool = 1;   // <-- Note the Bool option here !
        ci->getLangOpts().CPlusPlus = 1;
        ci->getLangOpts().CPlusPlus14 = 1;
        ci->getLangOpts().WChar = 1;

        ci->createFileManager();
        ci->createSourceManager(ci->getFileManager());//References getDiagnostics()
        this->createPreprocessor(ci);
        ci->createASTContext();
        return ci;
    }

    void createPreprocessor(clang::CompilerInstance *ci) {
        //https://stackoverflow.com/questions/30698834/clangheadersearch-search-path-ignored
        ci->createPreprocessor(clang::TU_Complete);
        std::vector<clang::DirectoryLookup> lookups;
        for (const std::string& path : std::vector<std::string> {
                "/usr/bin/../lib/gcc/x86_64-linux-gnu/10/../../../../include/c++/10",
                "/usr/bin/../lib/gcc/x86_64-linux-gnu/10/../../../../include/x86_64-linux-gnu/c++/10",
                "/usr/bin/../lib/gcc/x86_64-linux-gnu/10/../../../../include/c++/10/backward",
                "/usr/local/include",
                "/usr/lib/llvm-11/lib/clang/11.0.0/include",
                "/usr/include/x86_64-linux-gnu",
                "/usr/include"
        }) {
            llvm::Expected<clang::DirectoryEntryRef> dirRef = ci->getFileManager().getDirectoryRef(path);
            clang::DirectoryEntryRef ref = dirRef.get();
            auto lookup = clang::DirectoryLookup(ref, clang::SrcMgr::CharacteristicKind::C_User,false);
            if (!lookup.getDir())
                std::cout << "Cannot lookup user defined dir" << std::endl;
            lookups.emplace_back(lookup);
        }
        ci->getPreprocessor().getHeaderSearchInfo().SetSearchPaths(lookups, 0, 0, false);
    }

    
public:
    CompilerWrapper() {
        this->compilerInstance = this->getCompilerInstance();
        this->rewriter = new clang::Rewriter();
    }

    CompilerWrapper* addPragmaHandler(const std::string& ns, clang::PragmaHandler *pragmaHandler) {
        compilerInstance->getPreprocessor().AddPragmaHandler(ns, pragmaHandler);
        return this;
    }

    CompilerWrapper* addPragmaHandler(clang::PragmaHandler *pragmaHandler) {
        compilerInstance->getPreprocessor().AddPragmaHandler(pragmaHandler);
        return this;
    }

    CompilerWrapper* setASTConsumer(clang::ASTConsumer *consumer) {
        this->astConsumer = consumer;
        return this;
    }

    clang::Rewriter* getRewriter() {
        return this->rewriter;
    }

    void parseFile(std::string filename) {
        rewriter->setSourceMgr(compilerInstance->getSourceManager(), compilerInstance->getLangOpts());
        auto fileOrError = this->compilerInstance->getFileManager().getFile(filename);
        if (!fileOrError) {
            llvm::errs() << "File not found: " << fileOrError.getError().message();
            return;
        }
        const clang::FileEntry *file = fileOrError.get();
        clang::FileID mainFileID = this->compilerInstance->getSourceManager()
                .createFileID(file, clang::SourceLocation(), clang::SrcMgr::C_User);
        this->compilerInstance->getSourceManager().setMainFileID(mainFileID);
        this->compilerInstance->getPreprocessor().getPreprocessorOpts().SingleFileParseMode = true;
        this->compilerInstance->getDiagnosticClient()
                .BeginSourceFile(this->compilerInstance->getLangOpts(), nullptr);
        clang::ParseAST(compilerInstance->getPreprocessor(),
                        new clang::ASTConsumer(),
                        compilerInstance->getASTContext());

        for (auto i = this->compilerInstance->getSourceManager().fileinfo_begin(); i != this->compilerInstance->getSourceManager().fileinfo_end(); ++i) {
            const clang::FileEntry *fileEntry = i->getFirst();
            clang::FileID fileId = this->compilerInstance->getSourceManager().getOrCreateFileID(fileEntry, clang::SrcMgr::C_User);
            std::string rewrittenFilename = fileEntry->getName();
            std::string rewrittenSource = std::string(rewriter->getEditBuffer(fileId).begin(), rewriter->getEditBuffer(fileId).end());
            std::unique_ptr<llvm::MemoryBuffer> fileContentsBuf = llvm::MemoryBuffer::getMemBufferCopy(rewrittenSource);
            this->compilerInstance->getPreprocessorOpts().addRemappedFile(filename, fileContentsBuf.release());
        }

        this->createPreprocessor(compilerInstance);
        this->compilerInstance->getPreprocessor().getPreprocessorOpts().SingleFileParseMode = false;
        compilerInstance->createASTContext();
        clang::ParseAST(compilerInstance->getPreprocessor(),
                        this->astConsumer,
                        compilerInstance->getASTContext());
    }

};
