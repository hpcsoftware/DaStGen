#include <utility>
#include <vector>
#include <clang/Tooling/Tooling.h>
#include "PragmaHandler.cpp"
#include "CompilerWrapper.cpp"
#include "ClangAstConsumer.cpp"
#include "../../domain/DataRepresentation.cpp"
#include "../SourceParser.cpp"

class ParseDastgenPragmasAction : public clang::ASTFrontendAction {
public:
    ParseDastgenPragmasAction(std::string filename, std::map<std::string, std::string> *rewrittenSources) :
        _filename(std::move(filename)),
        _rewrittenSources(rewrittenSources) {}
protected:
    std::string _filename;
    std::map<std::string, std::string> *_rewrittenSources;
    clang::Rewriter *rewriter = new clang::Rewriter();

    bool BeginSourceFileAction(clang::CompilerInstance &CI) override {
        rewriter->setSourceMgr(CI.getSourceManager(), CI.getLangOpts());
        CI.getDiagnostics().setClient(new clang::IgnoringDiagConsumer());
        CI.getPreprocessor().AddPragmaHandler(new AnnotationAdder("dastgen", rewriter));
        CI.getPreprocessor().getPreprocessorOpts().SingleFileParseMode = true;

        auto fileOrError = CI.getFileManager().getFile(_filename);
        if (!fileOrError) {
            llvm::errs() << "File not found: " << fileOrError.getError().message();
            return false;
        }
        const clang::FileEntry *file = fileOrError.get();
        clang::FileID mainFileID = CI.getSourceManager()
                .createFileID(file, clang::SourceLocation(), clang::SrcMgr::C_User);
        CI.getSourceManager().setMainFileID(mainFileID);
        return true;
    }

    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
            clang::CompilerInstance &Compiler, llvm::StringRef InFile) {
        return std::make_unique<clang::ASTConsumer>();
    }

    void EndSourceFileAction() override {
        for (auto i = this->rewriter->getSourceMgr().fileinfo_begin(); i != this->rewriter->getSourceMgr().fileinfo_end(); ++i) {
            const clang::FileEntry *fileEntry = i->getFirst();
            clang::FileID fileId = this->rewriter->getSourceMgr().getOrCreateFileID(fileEntry, clang::SrcMgr::C_User);
            std::string rewrittenFilename = fileEntry->getName();
            std::string rewrittenSource = std::string(rewriter->getEditBuffer(fileId).begin(), rewriter->getEditBuffer(fileId).end());
            if (rewrittenSource.empty()) continue;
            (*_rewrittenSources)[rewrittenFilename] = rewrittenSource;
        }
    }
};

class ConsumeASTAction : public clang::ASTFrontendAction {
public:
    explicit ConsumeASTAction(std::string filename,
                              std::map<std::string, std::string> *rewrittenSources,
                              std::vector<DataClass*> *dataClasses,
                              std::vector<Enum *> *enums) :
        _filename(std::move(filename)),
        _rewrittenSources(rewrittenSources),
        _dataClasses(dataClasses),
        _enums(enums) {}
protected:
    std::string _filename;
    std::map<std::string, std::string> *_rewrittenSources;
    std::vector<DataClass *> *_dataClasses;
    std::vector<Enum *> *_enums;
    bool BeginSourceFileAction(clang::CompilerInstance &CI) override {
        for (auto &pair : *_rewrittenSources) {
            std::unique_ptr<llvm::MemoryBuffer> fileContentsBuf = llvm::MemoryBuffer::getMemBufferCopy(pair.second);
            CI.getPreprocessorOpts().addRemappedFile(pair.first, fileContentsBuf.release());
        }
        auto fileOrError = CI.getFileManager().getFile(_filename);
        if (!fileOrError) {
            llvm::errs() << "File not found: " << fileOrError.getError().message();
            return false;
        }
        const clang::FileEntry *file = fileOrError.get();
        clang::FileID mainFileID = CI.getSourceManager()
                .createFileID(file, clang::SourceLocation(), clang::SrcMgr::C_User);
        CI.getSourceManager().setMainFileID(mainFileID);
        CI.createPreprocessor(clang::TU_Complete);
        return true;
    }

    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
            clang::CompilerInstance &Compiler, llvm::StringRef InFile) {
        return std::make_unique<ClangAstConsumer>(_dataClasses, _enums);
    }
};

class ClangSourceParserV2 : public SourceParser {
private:
    const std::string dastgenPragmaPrefix = "dastgen";

    void removeDuplicateClasses(std::vector<DataClass *> *classes) {
        std::vector<DataClass *> uniqueClasses;
        std::set<std::string> names;
        for (auto dataClass : *classes) {
            std::string name;
            for (const auto& ns : dataClass->namespaces) name += ns;
            name += dataClass->name;
            if (names.count(name) == 1) continue;
            uniqueClasses.emplace_back(dataClass);
            names.insert(name);
        }
        classes->clear();
        classes->insert(classes->end(), uniqueClasses.begin(), uniqueClasses.end());
    }

public:
    std::vector<DataClass *> parseSourceFiles(int filesCount, char** filenames) override {
        std::map<std::string, std::string> *rewrittenSources = new std::map<std::string, std::string>();
        std::vector<DataClass *> *dataClasses = new std::vector<DataClass *>();
        std::vector<Enum*> *enums = new std::vector<Enum*>();

        for (int i = 0; i < filesCount; i++) {
            clang::tooling::runToolOnCode(std::make_unique<ParseDastgenPragmasAction>(std::string(filenames[i]), rewrittenSources), "");
        }

        for (int i = 0; i < filesCount; i++) {
            clang::tooling::runToolOnCode(std::make_unique<ConsumeASTAction>(std::string(filenames[i]), rewrittenSources, dataClasses, enums), "");
        }

        this->removeDuplicateClasses(dataClasses);
        return *dataClasses;
    }

};
