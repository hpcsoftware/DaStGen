#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Sema/Sema.h"
#include <iostream>
#include <utility>
#include "clang/Rewrite/Core/Rewriter.h"

class AbstractPragmaHandler : public clang::PragmaHandler {
private:

    const std::set<std::string> nonSeparateTokens {
        "-"
    };

    bool isStandaloneToken(const std::string& token) {
        return this->nonSeparateTokens.count(token) == 0;
    }

protected:
    virtual void onParsedPragma(clang::Preprocessor &PP,
                                clang::PragmaIntroducer Introducer,
                                clang::Token &LastTok,
                                std::string pragmaContents) = 0;

public:
    AbstractPragmaHandler(std::string pragmaPrefix)
        : PragmaHandler(pragmaPrefix) { }

    void HandlePragma(clang::Preprocessor &PP, clang::PragmaIntroducer Introducer,
                      clang::Token &PragmaTok) override {
        std::string pragmaContents = "";
        clang::Token Tok;
        PP.Lex(Tok);
        do {
            std::string token = PP.getSpelling(Tok);
            pragmaContents += token;
            if (this->isStandaloneToken(token)) pragmaContents += " ";
            PP.Lex(Tok);
        } while (Tok.isNot(clang::tok::eod));
        if (std::isspace(pragmaContents[pragmaContents.size()-1])) {
            pragmaContents.pop_back();
        }
        this->onParsedPragma(PP, Introducer, Tok, pragmaContents);
    }

};

class AnnotationAdder : public AbstractPragmaHandler {
private:
    clang::Rewriter *rewriter;

public:
    AnnotationAdder(std::string pragmaPrefix, clang::Rewriter *rewriter1)
        : AbstractPragmaHandler(std::move(pragmaPrefix)),
            rewriter(rewriter1) {}

protected:

    void onParsedPragma(clang::Preprocessor &PP, clang::PragmaIntroducer Introducer,
                        clang::Token &LastTok, std::string pragmaContents) override {
        clang::SourceRange range(Introducer.Loc, LastTok.getLocation());
        std::string replacementText = this->getReplacementText(pragmaContents);
        rewriter->ReplaceText(range, replacementText);
    }

    std::string getReplacementText(std::string pragmaContents) {
        return "__attribute__((annotate(\"" + pragmaContents + "\")))";
    }
};

class TokenLevelPragmaHandler : public AbstractPragmaHandler {

public:
    TokenLevelPragmaHandler(std::string pragmaPrefix)
        : AbstractPragmaHandler(pragmaPrefix) {}

protected:
    clang::Token buildSimpleToken(clang::tok::TokenKind kind, clang::SourceLocation &loc) {
        clang::Token t{};
        t.startToken();
        t.setKind(kind);
        t.setLocation(loc);
        return t;
    }

    clang::Token buildIdentifierToken(clang::Preprocessor &PP, const std::string& name, clang::SourceLocation &loc) {
        clang::Token t{};
        t.startToken();
        t.setKind(clang::tok::identifier);
        t.setLocation(loc);
        clang::IdentifierInfo& info = PP.getIdentifierTable().get(name, clang::tok::identifier);
        t.setIdentifierInfo(&info);
        return t;
    }

    clang::Token buildStringLiteral(clang::Preprocessor &PP, const std::string& contents, clang::SourceLocation &loc) {
        clang::Token t{};
        t.startToken();
        t.setKind(clang::tok::string_literal);
        std::string fill = "\"" + contents + "\"";
        llvm::StringRef r = llvm::StringRef(fill);
        PP.CreateString(r, t);
        t.setLocation(loc);
        return t;
    }

    void onParsedPragma(clang::Preprocessor &PP, clang::PragmaIntroducer Introducer,
                        clang::Token &LastTok, std::string pragmaContents) override {
        auto tokenArr = std::make_unique<clang::Token[]>(9);
        auto i = 0;
        auto sourceLoc = LastTok.getLocation();
        tokenArr[i++] = buildSimpleToken(clang::tok::kw___attribute, sourceLoc);
        tokenArr[i++] = buildSimpleToken(clang::tok::l_paren, sourceLoc);
        tokenArr[i++] = buildSimpleToken(clang::tok::l_paren, sourceLoc);
        tokenArr[i++] = buildIdentifierToken(PP, "annotate", sourceLoc);
        tokenArr[i++] = buildSimpleToken(clang::tok::l_paren, sourceLoc);
        tokenArr[i++] = buildStringLiteral(PP, pragmaContents, sourceLoc);
        tokenArr[i++] = buildSimpleToken(clang::tok::r_paren, sourceLoc);
        tokenArr[i++] = buildSimpleToken(clang::tok::r_paren, sourceLoc);
        tokenArr[i++] = buildSimpleToken(clang::tok::r_paren, sourceLoc);
        PP.EnterTokenStream(std::move(tokenArr), i, false, true);
    }

};