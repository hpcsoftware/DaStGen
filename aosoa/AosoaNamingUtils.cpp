#include <string>
#include "../domain/DataRepresentation.cpp"

#ifndef __DASTGEN2_NamingUtils__
#define __DASTGEN2_NamingUtils__

class AosoaNamingUtils {
public:
    std::string getArrayName() {
        return "arr";
    }

    std::string getArrayName(ClassField *classField) {
        return "arrOf" + classField->name;
    }

    std::string getListName() {
        return "list";
    }

    std::string getListName(ClassField *classField) {
        return "listOf" + classField->name;
    }

    std::string getGetSizeMethodName() {
        return "getSize";
    }

    std::string getLoadFromMethodName(DataClass *argumentDataClass) {
        return "loadFrom" + argumentDataClass->name;
    }

    std::string getLoadFromMethodArgName() {
        return "other";
    };

    std::string getSizeParamName() {
        return "size";
    }
};

#endif