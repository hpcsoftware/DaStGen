#include <stdexcept>
#include <utility>
#include <vector>

#include "../domain/DataRepresentation.cpp"
#include "../domain/LogicRepresentation.cpp"
#include "../domain/HighLevelFeatures.cpp"
#include "./AosoaNamingUtils.cpp"

class ArrayOfStructuresGenerator {
private:
    AosoaNamingUtils namingUtils;

    void createArrayClassField(DataClass *structureClass, DataClass *parentClass) {
        ClassField *classField = new ClassField();
        classField->name = this->namingUtils.getArrayName();
        classField->accessModifier = AccessModifier::PUBLIC;
        classField->parentClass = parentClass;
        classField->type = new ConstSizeArrayDataType(new DataClassDataType(structureClass), std::move(parentClass->parameters[0]->getName()));
        parentClass->fields.emplace_back(classField);
    }

    void createGetSizeMethod(DataClass *destClass) {
        ClassMethod *getSizeMethod = new ClassMethod();
        getSizeMethod->accessModifier = AccessModifier::PUBLIC;
        getSizeMethod->name = this->namingUtils.getGetSizeMethodName();
        getSizeMethod->returnType = destClass->parameters[0]->getType();
        getSizeMethod->methodStatements.emplace_back(new ReturnStatement(new ConstExpr(destClass->parameters[0]->getName())));
        destClass->methods.emplace_back(getSizeMethod);
    }

    ClassMethod *createEmptyLoadFromMethod(DataClass *fromClass, DataClass *destClass) {
        ClassMethod *copyMethod = new ClassMethod();
        copyMethod->accessModifier = AccessModifier::PUBLIC;
        copyMethod->name = this->namingUtils.getLoadFromMethodName(fromClass);
        copyMethod->returnType = new PrimitiveData(PrimitiveDataType::VOID);
        MethodArgument *methodArgument = new MethodArgument();
        methodArgument->name = this->namingUtils.getLoadFromMethodArgName();
        methodArgument->type = new DataClassDataType(fromClass);
        copyMethod->methodArguments.emplace_back(methodArgument);
        destClass->methods.emplace_back(copyMethod);
        return copyMethod;
    }

public:
    DataClass *getArrayOfStructures(DataClass *structureClass) {
        DataClass *ctAos = new DataClass();
        ctAos->name = "ArrayOf" + structureClass->name;
        ctAos->namespaces = structureClass->namespaces;
        const std::string sizeParameterName = this->namingUtils.getSizeParamName();
        ctAos->parameters.emplace_back(new DataClassParameter(PrimitiveDataType::INT, sizeParameterName));
        this->createArrayClassField(structureClass, ctAos);
        this->createGetSizeMethod(ctAos);
        return ctAos;
    }

    void createLoadFromLoS(DataClass *losClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(losClass, destClass);
        IVariableAccess *listAccess = new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), losClass->fields[0]->name);
        IVariableAccess *arrAccess = new ClassFieldVariableAccess(new LocalVariableAccess(destClass->fields[0]->name));
        loadMethod->methodStatements.emplace_back(new ListLoadToArray(listAccess, arrAccess));
    }

    void createLoadFromSoA(DataClass *structureClass, DataClass *soaClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(soaClass, destClass);
        LocalVariableAccess *iterCounter = new LocalVariableAccess("i");
        ClassInstanceMethodInvocation *arrSize = new ClassInstanceMethodInvocation(this->namingUtils.getGetSizeMethodName(), std::vector<IExpression *>());
        std::vector<IStatement *> loopStatements = std::vector<IStatement *>();
        for (ClassField *structureClassField : structureClass->fields) {
            IVariableAccess *arrWriteAccess = new ArrayVariableAccess(new ClassFieldVariableAccess(new LocalVariableAccess(destClass->fields[0]->name)), iterCounter);
            IVariableAccess *arrReadAccess = new ArrayVariableAccess(new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), this->namingUtils.getArrayName(structureClassField)), iterCounter);
            IStatement *valueAssignment = new ValueAssignment(new ObjectFieldVariableAccess(arrWriteAccess, structureClassField->name), arrReadAccess);
            loopStatements.emplace_back(valueAssignment);
        }
        loadMethod->methodStatements.emplace_back(new LoopStatement(iterCounter, arrSize, loopStatements));
    }

    void createLoadFromSoL(DataClass *structureClass, DataClass *solClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(solClass, destClass);
        LocalVariableAccess *iterCounter = new LocalVariableAccess("i");
        ClassInstanceMethodInvocation *arrSize = new ClassInstanceMethodInvocation(this->namingUtils.getGetSizeMethodName(), std::vector<IExpression *>());
        std::vector<IStatement *> loopStatements = std::vector<IStatement *>();
        for (ClassField *structureClassField : structureClass->fields) {
            //this->arr[0].x = other.listOfx[0]
            IVariableAccess *thisArrItemAccess = new ObjectFieldVariableAccess(new ArrayVariableAccess(new ClassFieldVariableAccess(new LocalVariableAccess(destClass->fields[0]->name)), iterCounter), structureClassField->name);
            IVariableAccess *otherListItemAccess = new ListItemAccess(new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), this->namingUtils.getListName(structureClassField)), iterCounter);
            IStatement *valueAssignment = new ValueAssignment(thisArrItemAccess, otherListItemAccess);
            loopStatements.emplace_back(valueAssignment);
        }
        loadMethod->methodStatements.emplace_back(new LoopStatement(iterCounter, arrSize, loopStatements));
    }
};
