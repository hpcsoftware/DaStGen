#include "../transformation/Transformation.cpp"
#include "./ArrayOfStructuresGenerator.cpp"
#include "./ListOfStructuresGenerator.cpp"
#include "./StructureOfArraysGenerator.cpp"
#include "./StructureOfListsGenerator.cpp"

class AosoaTransformationHandler : public ITransformation {
private:
    const std::string aosoaAnnotation = "aosoa";

    ArrayOfStructuresGenerator aosGen;
    ListOfStructuresGenerator losGen;
    StructureOfArraysGenerator soaGen;
    StructureOfListsGenerator solGen;

public:
    bool supports(DataClass *dataClass) override {
        for (auto annotation : dataClass->annotations) {
            if (annotation->tokens[0] != aosoaAnnotation) continue;
            return true;
        }
        return false;
    }

    std::vector<DataClass *> transform(DataClass *sourceClass) override {
        auto aos = aosGen.getArrayOfStructures(sourceClass);
        auto los = losGen.getListOfStructures(sourceClass);
        auto soa = soaGen.getStructureOfArrays(sourceClass);
        auto sol = solGen.getStructureOfLists(sourceClass);

        aosGen.createLoadFromLoS(los, aos);
        aosGen.createLoadFromSoA(sourceClass, soa, aos);
        aosGen.createLoadFromSoL(sourceClass, sol, aos);

        losGen.createLoadFromAoS(aos, los);
        losGen.createLoadFromSoA(sourceClass, soa, los);
        losGen.createLoadFromSoL(sourceClass, sol, los);

        soaGen.createLoadFromAoS(sourceClass, aos, soa);
        soaGen.createLoadFromLoS(sourceClass, los, soa);
        soaGen.createLoadFromSoL(sourceClass, sol, soa);

        solGen.createLoadFromAoS(sourceClass, aos, sol);
        solGen.createLoadFromLoS(sourceClass, los, sol);
        solGen.createLoadFromSoA(sourceClass, soa, sol);

        return std::vector<DataClass *>{aos, los, soa, sol};
    }
};
