#include <stdexcept>
#include <vector>

#include "../domain/DataRepresentation.cpp"
#include "../domain/LogicRepresentation.cpp"
#include "../domain/HighLevelFeatures.cpp"
#include "./AosoaNamingUtils.cpp"

class StructureOfListsGenerator {
private:

    AosoaNamingUtils namingUtils;
    PrimitiveData *indexDataType = new PrimitiveData(PrimitiveDataType::INT);

    void createListClassFields(DataClass *structureClass, DataClass *parentClass) {
        for (ClassField *structureClassField : structureClass->fields) {
            ClassField *classField = new ClassField();
            classField->name = this->namingUtils.getListName(structureClassField);
            classField->accessModifier = AccessModifier::PUBLIC;
            classField->parentClass = parentClass;
            classField->type = new ListDataType(structureClassField->type);
            parentClass->fields.emplace_back(classField);
        }
    }

    void createGetSizeMethod(DataClass *destClass) {
        ClassMethod *getSizeMethod = new ClassMethod();
        getSizeMethod->accessModifier = AccessModifier::PUBLIC;
        getSizeMethod->name = this->namingUtils.getGetSizeMethodName();
        getSizeMethod->returnType = new PrimitiveData(PrimitiveDataType::INT);
        getSizeMethod->methodStatements.emplace_back(new ReturnStatement(new ListGetSize(new ClassFieldVariableAccess(new LocalVariableAccess(destClass->fields[0]->name)))));
        destClass->methods.emplace_back(getSizeMethod);
    }

    ClassMethod *createEmptyLoadFromMethod(DataClass *fromClass, DataClass *destClass) {
        ClassMethod *copyMethod = new ClassMethod();
        copyMethod->accessModifier = AccessModifier::PUBLIC;
        copyMethod->name = this->namingUtils.getLoadFromMethodName(fromClass);
        copyMethod->returnType = new PrimitiveData(PrimitiveDataType::VOID);
        MethodArgument *methodArgument = new MethodArgument();
        methodArgument->name = this->namingUtils.getLoadFromMethodArgName();
        methodArgument->type = new DataClassDataType(fromClass);
        copyMethod->methodArguments.emplace_back(methodArgument);
        destClass->methods.emplace_back(copyMethod);
        return copyMethod;
    }

public:
    DataClass *getStructureOfLists(DataClass *structureClass) {
        DataClass *ctAos = new DataClass();
        ctAos->name = "StructureOfListsOf" + structureClass->name;
        ctAos->namespaces = structureClass->namespaces;
        ctAos->highLevelFeatures.emplace(HighLevelFeatures::LIST);
        this->createListClassFields(structureClass, ctAos);
        this->createGetSizeMethod(ctAos);
        return ctAos;
    }

    void createLoadFromAoS(DataClass *structureClass, DataClass *aosClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(aosClass, destClass);
        LocalVariableAccess *iterCounter = new LocalVariableAccess("i");
        ClassInstanceMethodInvocation *arrSize = new ClassInstanceMethodInvocation(this->namingUtils.getGetSizeMethodName(), std::vector<IExpression *>());
        std::vector<IStatement *> loopStatements = std::vector<IStatement *>();
        for (ClassField *structureClassField : structureClass->fields) {
            IVariableAccess *arrReadAccess = new ObjectFieldVariableAccess(new ArrayVariableAccess(new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), aosClass->fields[0]->name), iterCounter), structureClassField->name);
            IStatement *valueAssignment = new ListItemAssignment(new ClassFieldVariableAccess(new LocalVariableAccess(this->namingUtils.getListName(structureClassField))), arrReadAccess, iterCounter);
            loopStatements.emplace_back(valueAssignment);
        }
        loadMethod->methodStatements.emplace_back(new LoopStatement(iterCounter, arrSize, loopStatements));
    }

    void createLoadFromLoS(DataClass *structureClass, DataClass *losClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(losClass, destClass);
        LocalVariableAccess *iterCounter = new LocalVariableAccess("i");
        ClassInstanceMethodInvocation *arrSize = new ClassInstanceMethodInvocation(this->namingUtils.getGetSizeMethodName(), std::vector<IExpression *>());
        std::vector<IStatement *> loopStatements = std::vector<IStatement *>();
        for (ClassField *structureClassField : structureClass->fields) {
            IVariableAccess *listReadAccess = new ObjectFieldVariableAccess(new ListItemAccess(new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), losClass->fields[0]->name), iterCounter), structureClassField->name);
            IStatement *valueAssignment = new ListItemAssignment(new ClassFieldVariableAccess(new LocalVariableAccess(this->namingUtils.getListName(structureClassField))), listReadAccess, iterCounter);
            loopStatements.emplace_back(valueAssignment);
        }
        loadMethod->methodStatements.emplace_back(new LoopStatement(iterCounter, arrSize, loopStatements));
    }

    void createLoadFromSoA(DataClass *structureClass, DataClass *soaClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(soaClass, destClass);
        MethodInvocation *arrSize = new MethodInvocation(new LocalVariableAccess(loadMethod->methodArguments[0]->name), this->namingUtils.getGetSizeMethodName(), std::vector<IExpression *>());
        for (ClassField *structureClassField : structureClass->fields) {
            IVariableAccess *arrReadAccess = new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), this->namingUtils.getArrayName(structureClassField));
            IVariableAccess *listWriteAccess = new ClassFieldVariableAccess(new LocalVariableAccess(this->namingUtils.getListName(structureClassField)));
            loadMethod->methodStatements.emplace_back(new ListLoadFromArray(listWriteAccess, arrReadAccess, arrSize));
        }
    }
};
