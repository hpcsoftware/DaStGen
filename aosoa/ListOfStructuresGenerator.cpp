#include <stdexcept>
#include <utility>
#include <vector>

#include "../domain/DataRepresentation.cpp"
#include "../domain/LogicRepresentation.cpp"
#include "../domain/HighLevelFeatures.cpp"
#include "./AosoaNamingUtils.cpp"

class ListOfStructuresGenerator {
private:

    AosoaNamingUtils namingUtils;
    PrimitiveData *indexDataType = new PrimitiveData(PrimitiveDataType::INT);

    void createListClassField(DataClass *structureClass, DataClass *parentClass) {
        ClassField *classField = new ClassField();
        classField->name = this->namingUtils.getListName();
        classField->accessModifier = AccessModifier::PUBLIC;
        classField->parentClass = parentClass;
        classField->type = new ListDataType(new DataClassDataType(structureClass));
        parentClass->fields.emplace_back(classField);
    }

    void createGetSizeMethod(DataClass *destClass) {
        ClassMethod *getSizeMethod = new ClassMethod();
        getSizeMethod->accessModifier = AccessModifier::PUBLIC;
        getSizeMethod->name = this->namingUtils.getGetSizeMethodName();
        getSizeMethod->returnType = this->indexDataType;
        getSizeMethod->methodStatements.emplace_back(new ReturnStatement(new MethodInvocation(new ClassFieldVariableAccess(new LocalVariableAccess(destClass->fields[0]->name)), "size", std::vector<IExpression *>())));
        destClass->methods.emplace_back(getSizeMethod);
    }

    ClassMethod *createEmptyLoadFromMethod(DataClass *fromClass, DataClass *destClass) {
        ClassMethod *copyMethod = new ClassMethod();
        copyMethod->accessModifier = AccessModifier::PUBLIC;
        copyMethod->name = this->namingUtils.getLoadFromMethodName(fromClass);
        copyMethod->returnType = new PrimitiveData(PrimitiveDataType::VOID);
        MethodArgument *methodArgument = new MethodArgument();
        methodArgument->name = this->namingUtils.getLoadFromMethodArgName();
        methodArgument->type = new DataClassDataType(fromClass);
        copyMethod->methodArguments.emplace_back(methodArgument);
        destClass->methods.emplace_back(copyMethod);
        return copyMethod;
    }

public:
    DataClass *getListOfStructures(DataClass *structureClass) {
        DataClass *ctAos = new DataClass();
        ctAos->name = "ListOf" + structureClass->name;
        ctAos->namespaces = structureClass->namespaces;
        ctAos->highLevelFeatures.emplace(HighLevelFeatures::LIST);
        this->createListClassField(structureClass, ctAos);
        this->createGetSizeMethod(ctAos);
        return ctAos;
    }

    void createLoadFromAoS(DataClass *aosClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(aosClass, destClass);
        IVariableAccess *listAccess = new ClassFieldVariableAccess(new LocalVariableAccess(destClass->fields[0]->name));
        IVariableAccess *arrAccess = new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), aosClass->fields[0]->name);
        loadMethod->methodStatements.emplace_back(new ListLoadFromArray(listAccess, arrAccess, new MethodInvocation(new LocalVariableAccess(loadMethod->methodArguments[0]->name), this->namingUtils.getGetSizeMethodName(), std::vector<IExpression*>())));
    }

    void createLoadFromSoA(DataClass *structureClass, DataClass *soaClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(soaClass, destClass);
        LocalVariableAccess *iterCounter = new LocalVariableAccess("i");
        ClassInstanceMethodInvocation *arrSize = new ClassInstanceMethodInvocation(this->namingUtils.getGetSizeMethodName(), std::vector<IExpression *>());
        std::vector<IStatement *> loopStatements = std::vector<IStatement *>();
        for (ClassField *structureClassField : structureClass->fields) {
            IVariableAccess *arrWriteAccess = new ListItemAccess(new ClassFieldVariableAccess(new LocalVariableAccess(destClass->fields[0]->name)), iterCounter);
            IVariableAccess *arrReadAccess = new ArrayVariableAccess(new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), structureClassField->name), iterCounter);
            IStatement *valueAssignment = new ValueAssignment(new ObjectFieldVariableAccess(arrWriteAccess, structureClassField->name), arrReadAccess);
            loopStatements.emplace_back(valueAssignment);
        }
        loadMethod->methodStatements.emplace_back(new LoopStatement(iterCounter, arrSize, loopStatements));
    }

    void createLoadFromSoL(DataClass *structureClass, DataClass *solClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(solClass, destClass);
        LocalVariableAccess *iterCounter = new LocalVariableAccess("i");
        ClassInstanceMethodInvocation *arrSize = new ClassInstanceMethodInvocation(this->namingUtils.getGetSizeMethodName(), std::vector<IExpression *>());
        std::vector<IStatement *> loopStatements = std::vector<IStatement *>();
        for (ClassField *structureClassField : structureClass->fields) {
            //this->list[0].x = other.listOfx[0]
            IVariableAccess *thisArrItemAccess = new ObjectFieldVariableAccess(new ListItemAccess(new ClassFieldVariableAccess(new LocalVariableAccess(destClass->fields[0]->name)), iterCounter), structureClassField->name);
            IVariableAccess *otherListItemAccess = new ListItemAccess(new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), this->namingUtils.getListName(structureClassField)), iterCounter);
            IStatement *valueAssignment = new ValueAssignment(thisArrItemAccess, otherListItemAccess);
            loopStatements.emplace_back(valueAssignment);
        }
        loadMethod->methodStatements.emplace_back(new LoopStatement(iterCounter, arrSize, loopStatements));
    }
};