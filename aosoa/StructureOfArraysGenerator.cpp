#include <stdexcept>
#include <utility>
#include <vector>

#include "../domain/DataRepresentation.cpp"
#include "../domain/LogicRepresentation.cpp"
#include "../domain/HighLevelFeatures.cpp"
#include "./AosoaNamingUtils.cpp"

class StructureOfArraysGenerator {
private:
    AosoaNamingUtils namingUtils;

    void createArrayClassFields(DataClass *structureClass, DataClass *parentClass) {
        for (ClassField *structureClassField : structureClass->fields) {
            ClassField *classField = new ClassField();
            classField->name = this->namingUtils.getArrayName(structureClassField);
            classField->accessModifier = AccessModifier::PUBLIC;
            classField->parentClass = parentClass;
            classField->type = new ConstSizeArrayDataType(structureClassField->type, std::move(parentClass->parameters[0]->getName()));
            parentClass->fields.emplace_back(classField);
        }
    }

    void createGetSizeMethod(DataClass *destClass) {
        ClassMethod *getSizeMethod = new ClassMethod();
        getSizeMethod->accessModifier = AccessModifier::PUBLIC;
        getSizeMethod->name = this->namingUtils.getGetSizeMethodName();
        getSizeMethod->returnType = destClass->parameters[0]->getType();
        getSizeMethod->methodStatements.emplace_back(new ReturnStatement(new ConstExpr(destClass->parameters[0]->getName())));
        destClass->methods.emplace_back(getSizeMethod);
    }

    ClassMethod *createEmptyLoadFromMethod(DataClass *fromClass, DataClass *destClass) {
        ClassMethod *copyMethod = new ClassMethod();
        copyMethod->accessModifier = AccessModifier::PUBLIC;
        copyMethod->name = this->namingUtils.getLoadFromMethodName(fromClass);
        copyMethod->returnType = new PrimitiveData(PrimitiveDataType::VOID);
        MethodArgument *methodArgument = new MethodArgument();
        methodArgument->name = this->namingUtils.getLoadFromMethodArgName();
        methodArgument->type = new DataClassDataType(fromClass);
        copyMethod->methodArguments.emplace_back(methodArgument);
        destClass->methods.emplace_back(copyMethod);
        return copyMethod;
    }

public:
    DataClass *getStructureOfArrays(DataClass *structureClass) {
        DataClass *ctAos = new DataClass();
        ctAos->name = "StructureOfArraysOf" + structureClass->name;
        ctAos->namespaces = structureClass->namespaces;
        const std::string sizeParameterName = this->namingUtils.getSizeParamName();
        ctAos->parameters.emplace_back(new DataClassParameter(PrimitiveDataType::INT, sizeParameterName));
        this->createArrayClassFields(structureClass, ctAos);
        this->createGetSizeMethod(ctAos);
        return ctAos;
    }

    void createLoadFromAoS(DataClass *structureClass, DataClass *aosClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(aosClass, destClass);
        LocalVariableAccess *iterCounter = new LocalVariableAccess("i");
        ClassInstanceMethodInvocation *arrSize = new ClassInstanceMethodInvocation(this->namingUtils.getGetSizeMethodName(), std::vector<IExpression *>());
        std::vector<IStatement *> loopStatements = std::vector<IStatement *>();
        for (ClassField *structureClassField : structureClass->fields) {
            IVariableAccess *arrReadAccess = new ObjectFieldVariableAccess(new ArrayVariableAccess(new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), aosClass->fields[0]->name), iterCounter), structureClassField->name);
            IVariableAccess *arrWriteAccess = new ArrayVariableAccess(new ClassFieldVariableAccess(new LocalVariableAccess(this->namingUtils.getArrayName(structureClassField))), iterCounter);
            IStatement *valueAssignment = new ValueAssignment(arrWriteAccess, arrReadAccess);
            loopStatements.emplace_back(valueAssignment);
        }
        loadMethod->methodStatements.emplace_back(new LoopStatement(iterCounter, arrSize, loopStatements));
    }

    void createLoadFromLoS(DataClass *structureClass, DataClass *losClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(losClass, destClass);
        LocalVariableAccess *iterCounter = new LocalVariableAccess("i");
        ClassInstanceMethodInvocation *arrSize = new ClassInstanceMethodInvocation(this->namingUtils.getGetSizeMethodName(), std::vector<IExpression *>());
        std::vector<IStatement *> loopStatements = std::vector<IStatement *>();
        for (ClassField *structureClassField : structureClass->fields) {
            IVariableAccess *listReadAccess = new ObjectFieldVariableAccess(new ListItemAccess(new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), losClass->fields[0]->name), iterCounter), structureClassField->name);
            IVariableAccess *arrWriteAccess = new ArrayVariableAccess(new ClassFieldVariableAccess(new LocalVariableAccess(this->namingUtils.getArrayName(structureClassField))), iterCounter);
            IStatement *valueAssignment = new ValueAssignment(arrWriteAccess, listReadAccess);
            loopStatements.emplace_back(valueAssignment);
        }
        loadMethod->methodStatements.emplace_back(new LoopStatement(iterCounter, arrSize, loopStatements));
    }

    void createLoadFromSoL(DataClass *structureClass, DataClass *solClass, DataClass *destClass) {
        ClassMethod *loadMethod = this->createEmptyLoadFromMethod(solClass, destClass);
        for (ClassField *structureClassField : structureClass->fields) {
            IVariableAccess *listReadAccess = new ObjectFieldVariableAccess(new LocalVariableAccess(loadMethod->methodArguments[0]->name), this->namingUtils.getListName(structureClassField));
            IVariableAccess *arrWriteAccess = new ClassFieldVariableAccess(new LocalVariableAccess(this->namingUtils.getArrayName(structureClassField)));
            loadMethod->methodStatements.emplace_back(new ListLoadToArray(listReadAccess, arrWriteAccess));
        }
    }
};
