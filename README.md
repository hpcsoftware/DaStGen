# DaStGen 2

The DaStGen 2 (Data Structure Generator 2) project is a source-to-source generator which focuses on automating the code generation process for high-performace applications.
Currently, it supports C++ source files as input, and C++ as the output language.

## Usage
```bash
> ./dastgen2 [filepaths...]
```

The output source files will be written to ```./out/generated```

DaStGen accepts one or more paths to C++ source files.
The contents of the files determine what classes are generated. 
See [Functionality](#functionality) for details.

Example input files can be found in ```./test-cases```

## Functionality
### #pragma dastgen compressed
DaStGen can generate a class with all of the fields compressed to a single byte array.
The original fields are accessed and updated by using the autogenerated getter and setter methods.
For example, given:
```cpp
class 
#pragma dastgen compressed
DataClass {
    bool a, ..., h;
}
```
DaStGen generates the following compressed class:
```cpp
class DataClass {
    unsigned char table[1];
    
    bool getA() { ... }
    bool setA(bool val) { ... }
    ...
}
```
In this particular example, the memory footprint is reduced by 87.5%
This comes at a cost of the additional bitshift operations necessary to retrieve the bit values from the array.

DaStGen supports different strategies for arranging the compressed variables inside the byte block.
The following compression strategies are available:

-  ```#pragma dastgen compressed opt_access``` optimises the arrangement for access speed.
This is achieved by minimising the amount of bit-shift operations necessary to retrieve all values.
In particular, this means that no value will be stored across multiple consecutive bytes unless its size mandates it.
   

-  ```#pragma dastgen compressed opt_packing``` optimises the arrangement for minimal memory footprint.
  This is achieved by minimising the amount of wasted space in the byte block. 
  In particular, this means that some variables might be stored across multiple bytes regardless of their size.


-  ```#pragma dastgen compressed opt_packing_budget <N>``` works similarly to the ```opt_packing``` strategy.
   The integer parameter ```N``` controls how many bits the optimiser is allowed trade off for a decrease in bit-shift operations.
   For ```N == 0```, this strategy is the same as ```opt_packing```.
   For ```N >> 0``` this strategy is the same as ```opt_access```.


-  ```#pragma dastgen compressed``` falls back to ```opt_packing```.

Other class-level pragmas related to compression are available: 
- ```#pragma dastgen chunk_size <N>``` specifies the size of the array members of the compressed bits store.
Supported values are ``8``, ``16``, ``32``, ``64``.
Higher sizes can be beneficial in reducing the number of variables which span across consecutive array members,
which in turns reduces the number of bitshift operations required for read and write access.
On the other hand, lower values help minimise the wasted memory.
When unspecified, the size ``8`` is used.


In addition, the following field-level pragmas are available:
- ```#pragma dastgen int_range <low bound> <high bound inclusive>``` compresses the values to the confines of the given range. 
  Positive and negative bounds are supported.
  Low bound must be numerically lower than high bound.
  All primitive integer-like data types are supported.
  

- ```#pragma dastgen float_standard <standard>``` compresses the values to the given precision standard.
  The defined standards ```float16```, ```float32```, ```float64```, ```float128``` are compliant with the IEEE 754 specification.
  All primitive float-like data types are supported.
  

- ```#pragma dastgen float_format <sign type> <exponent size> <exponent bias> <significand size>``` compresses the values to the given precision format.
  The value of ```sign type``` specifies the value + / - sign.
  Possible values are ```positive```, ```negative``` and ```mixed_sign```.
  The value of ```exponent size``` determines the size of the exponent in bits.
  The value of ```exponent bias``` determines the value of the exponent bias.
  The value of ```significand size``` determines the size of the significand (mantissa) in bits.
  For example, the IEEE754 ```float32``` is equivalent to ```mixed_sign 8 127 23```.
  

- ```#pragma dastgen ignored``` excludes an otherwise eligible field from being compressed.
  Any non-compressed fields are simply recreated in the compressed class.
  

### #pragma dastgen class_view
DaStGen can create a class, or classes, which contains a specified subset of fields (called a *class view*).
An instance of the original class can update and retrieve values from a class view by the autogenerated loader methods.
Furthermore, data transfer between different class views can also happen via the same mechanism.
For example, given:
```cpp
class 
#pragma dastgen class_view
DataClass {
    bool a;
    #pragma dastgen class_view_field NumbersOnly
    int i;
}
```
DaStGen generates the following class view:
```cpp
class NumbersOnlyDataClass {
    int i;
    
    void loadFrom(DataClass &val) { ... }
}
```
In this particular example, the ```bool a``` field is excluded from the class view as it is missing the required ```#pragma ...```.
Different pragmas exist for controlling the excluding/including behaviour.
For details, see ```./test-cases/class-view```

### #pragma dastgen aosoa
DaStGen can create a set of classes which store ordered collections (arrays or lists) of a given class.
Two storage structures are supported - array/list of structures, and structure of arrays/lists.
Data can be copied across the different storage structures via the autogenerated loader methods.
For example, given:
```cpp
class 
#pragma dastgen aosoa
DataClass {
    int a, b, c;
}
```
DaStGen generates the following array of structures:
```cpp
template<int size>
class ArrayOfDataClass {
    DataClass arr[size];
    
    void loadFrom(StructureOfArraysOfDataClass &a) { ... }
}
```
and the following structure of arrays:
```cpp
template<int size>
class StructureOfArraysOfDataClass {
    int arrOfa[size];
    int arrOfb[size];
    int arrOfc[size];
    
    void loadFrom(ArrayOfDataClass &a) { ... }
}
```
Currently, a total of 4 classes is generated (array of structures, list of structures, structure of arrays, and structure of lists), and copying between all of them is possible via the autogenerated loader methods.
For details, see ```./test-cases/aosoa```

### #pragma dastgen forward
DaStGen can chain transformations together.
This allows for for a more advanced code generation where new classes are derived based on previously generated classes.
For example, given:
```cpp
class
#pragma dastgen class_view
#pragma dastgen forward BooleansOnlyDataClass compressed
#pragma dastgen forward BooleansOnlyDataClass::b3 ignored
DataClass {
    #pragma dastgen class_view_field BooleansOnly
    bool b1, b2, b3;

    #pragma dastgen class_view_field IntegersOnly
    int i1, i2, i3;
};
```
DaStGen generates a total of 4 classes.
Most notably, it creates a compressed view ```CompressedBooleansOnlyDataClass``` which is a class derived from the generated class view ```BooleansOnlyDataClass```:
```cpp
class CompressedBooleansOnlyDataClass {
    unsigned char __table[1];
    bool b3;
    ...
}
```
This mechanism supports forwarding the DaStGen pragmas both to generated classes, as well as the fields of the generated classes.
In the example above, ```#pragma dastgen ignored``` is forwarded to the field ```bool b3``` of the ```BooleansOnlyDataClass``` which excludes it from the compression.
For details, see ```./test-cases/forward```

### #pragma dastgen substituted
DaStGen can change the types of the fields of generated classes.
This might be useful in certain scenarios, e.g. when a compressed class contains a nested class which is also compressed.
For example, given:
```cpp
class
#pragma dastgen compressed
ChildDataClass {
    bool a, b, c;
};

class
#pragma dastgen compressed
#pragma dastgen substituted
ParentDataClass {
    int d, e, f;

    #pragma dastgen substitute_with CompressedChildDataClass
    ChildDataClass child;
};
```
DaStGen will generate the following:
```cpp
class CompressedChildDataClass {
    unsigned char __table[1];
    ...
};

class CompressedParentDataClass {
    CompressedChildDataClass child;
    ...
};
```

Without the ```substitute_with``` pragma, ```CompressedParentDataClass``` would contain the original, uncompressed ```ChildDataClass```.
Thanks to this pragma, DaStGen knows that the type of the field should be changed to the compressed generated version of the class.
### #pragma dastgen nop
This pragma will try to recreate the input classes as closely as possible.
In addition, it will generate getter and setter methods for every fields in the class.
Its primary purpose is to be used internally for regression testing.
However, sometimes it might be useful to ingest an input class with this pragma to catch any potential issues (unsupported data types, circular dependencies, etc.) early.

## Building from source
```bash
> cmake . && make
```
The ```./dastgen2``` executable binary will be created.

This project depends on the  ```libclang-dev``` and ```libLLVM``` libraries,
as well as the ```cmake``` and ```make``` build tools.
To fetch these on a Ubuntu derivative, run:
```bash
> sudo apt-get install cmake make libclang-dev libLLVM
```
Furthermore, Google's [OR-Tools](https://developers.google.com/optimization) are required.
The binary version should be located in ``/opt/ortools`` for the CMake script to work.
