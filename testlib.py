import os
import shutil
import sys

import pathlib
root_path = pathlib.Path(__file__).parent.absolute()
root_path = str(root_path)

tests_dir_path = root_path + '/test-cases'
executable_path = root_path + '/cmake-build-clang/dastgen2'
generated_files_dir_path = root_path + '/out/generated'


def listdir(path):
    files_list = os.listdir(path)
    files_list.sort()
    files_list = [open('/'.join([path, f])) for f in files_list]
    return files_list


def clean_output():
    pathlib.Path(generated_files_dir_path).mkdir(parents=True, exist_ok=True)
    shutil.rmtree(generated_files_dir_path)
    pathlib.Path(generated_files_dir_path).mkdir(parents=True, exist_ok=True)


def run_dastgen(paths):
    clean_output()
    if not isinstance(paths, list):
        paths = [paths]
    paths = ' '.join(paths)
    command = f'{executable_path} -o {generated_files_dir_path} {paths}'
    return os.system(command)


def convert_file_to_nonempty_lines(file):
    lines = file.readlines()
    numbered_lines = []
    for i in range(len(lines)):
        numbered_lines.append((i, lines[i].strip()))
    numbered_lines = [line for line in numbered_lines if line[1].strip()]
    return numbered_lines


def compare_nonempty_lines(file_a, file_b):
    file_a = convert_file_to_nonempty_lines(file_a)
    file_b = convert_file_to_nonempty_lines(file_b)
    success = True
    for i in range(len(file_a)):
        if file_a[i][1] == file_b[i][1]: continue
        success = False
        print(f'Discrepancy {file_a[i][0]}: {file_a[i][1]} | {file_b[i][0]}: {file_b[i][1]}')
    return success


def run_test_case(input_file, expected_outputs_dir):
    clean_output()
    run_dastgen(input_file)
    expected_output_files = listdir(expected_outputs_dir)
    actual_output_files = listdir(generated_files_dir_path)
    for expected_file, actual_file in zip(expected_output_files, actual_output_files):
        successful = compare_nonempty_lines(expected_file, actual_file)
        if not successful:
            print(f'Failed output file {expected_file.name}')
            sys.exit(1)
        else:
            print(f'Test pass {expected_file.name}')
