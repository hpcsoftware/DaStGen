#include "./Transformation.cpp"
#include "../methods/GetterSetterGenerator.cpp"

class NoOpTransformation : public ITransformation {
private:
    const std::string noopAnnotationToken = "nop";
    GetterSetterGenerator generator;

public:
    bool supports(DataClass *dataClass) override {
        for (auto annotation : dataClass->annotations) {
            if (annotation->tokens[0] == noopAnnotationToken) return true;
        }
        return false;
    }

    std::vector<DataClass *> transform(DataClass *dataClass) override {
        std::vector<Annotation *> annotations;
        for (auto annotation : dataClass->annotations) {
            if (annotation->tokens[0] == noopAnnotationToken) continue;
            annotations.emplace_back(annotation);
        }
        dataClass->annotations = annotations;
        dataClass->originalSourceUnitPath = "";
        dataClass = generator.generateGettersSetters(dataClass);
        return std::vector<DataClass *>{dataClass};
    }
};
