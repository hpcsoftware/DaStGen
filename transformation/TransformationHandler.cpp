#include "./Transformation.cpp"
#include "./NopTransformation.cpp"
#include "../aosoa/AosoaTransformationHandler.cpp"
#include "../class-view/ClassViewGenerator.cpp"
#include "../memory/DataClassMemoryOptimiser.cpp"
#include "../forward/ForwardTransformation.cpp"
#include "../substitute/SubstituteTransformation.cpp"

class TransformationHandler {
private:
    SubstituteTypeAnnotationHandler substituteTypeAnnotationHandler;

    std::vector<ITransformation*> transformations {
        new AosoaTransformationHandler(),
        new ClassViewGenerator(),
        new DataClassMemoryOptimiser(),
        new NoOpTransformation()
    };

    ForwardAnnotationHandler forwardAnnotationHandler;

    std::vector<DataClass *> handleDataClass(DataClass *dataClass) {
        std::vector<DataClass *> outputClasses;
        for (auto transformation : transformations) {
            if (!transformation->supports(dataClass)) continue;
            std::vector<DataClass *> classes = transformation->transform(dataClass);
            outputClasses.insert(outputClasses.end(), classes.begin(), classes.end());
        }
        return outputClasses;
    }

    void handleForwardAnnotations(DataClass *sourceClass, const std::vector<DataClass *>& generatedClasses) {
        this->forwardAnnotationHandler.handleForwardAnnotations(sourceClass, generatedClasses);
    }

    std::tuple<std::vector<DataClass *>, std::vector<DataClass *>> handleTransformationsWithoutSubs(const std::vector<DataClass*>& dataClasses) {
        std::vector<DataClass *> generatedSubsClasses;
        std::vector<DataClass *> generatedNonSubsClasses;
        for (DataClass *dataClass : dataClasses) {
            if (substituteTypeAnnotationHandler.isSupported(dataClass)) continue;
            std::vector<DataClass *> classes = this->handleDataClass(dataClass);
            this->handleForwardAnnotations(dataClass, classes);
            for (auto generatedClass : classes) {
                if (substituteTypeAnnotationHandler.isSupported(generatedClass))
                    generatedSubsClasses.emplace_back(generatedClass);
                else
                    generatedNonSubsClasses.emplace_back(generatedClass);
            }
        }
        if (!generatedNonSubsClasses.empty()) {
            std::tuple<std::vector<DataClass *>, std::vector<DataClass *>> generatedGeneratedClasses = this->handleTransformationsWithoutSubs(generatedNonSubsClasses);
            std::vector<DataClass *> subsClasses = std::get<0>(generatedGeneratedClasses);
            generatedSubsClasses.insert(generatedSubsClasses.end(), subsClasses.begin(), subsClasses.end());
            std::vector<DataClass *> noSubsClasses = std::get<1>(generatedGeneratedClasses);
            generatedNonSubsClasses.insert(generatedNonSubsClasses.end(), noSubsClasses.begin(), noSubsClasses.end());
        }
        return std::make_tuple(generatedSubsClasses, generatedNonSubsClasses);
    }

    std::tuple<std::vector<DataClass *>, std::vector<DataClass *>> handleSubs(std::vector<DataClass *> &subsClasses, std::vector<DataClass *> allGeneratedClasses) {
        std::vector<DataClass *> subbedClasses;
        std::vector<DataClass *> notSubbedClasses;
        for (auto dataClass : subsClasses) {
            bool success = this->substituteTypeAnnotationHandler.handle(dataClass, allGeneratedClasses);
            if (success) subbedClasses.emplace_back(dataClass);
            else notSubbedClasses.emplace_back(dataClass);
        }
        return std::make_tuple(notSubbedClasses, subbedClasses);
    }

public:
    std::vector<DataClass *> handleTransformations(const std::vector<DataClass*>& dataClasses) {
        std::vector<DataClass *> substituteClasses;
        std::vector<DataClass *> nonSubstituteClasses;

        for (auto dataClass : dataClasses) {
            if (substituteTypeAnnotationHandler.isSupported(dataClass))
                substituteClasses.emplace_back(dataClass);
            else
                nonSubstituteClasses.emplace_back(dataClass);
        }
        std::tuple<std::vector<DataClass *>, std::vector<DataClass *>> generatedGeneratedClasses
            = this->handleTransformationsWithoutSubs(nonSubstituteClasses);
        std::vector<DataClass *> subsClasses = std::get<0>(generatedGeneratedClasses);
        substituteClasses.insert(substituteClasses.end(), subsClasses.begin(), subsClasses.end());
        nonSubstituteClasses = std::get<1>(generatedGeneratedClasses);

        bool carryOn;
        do {
            carryOn = false;
            std::tuple<std::vector<DataClass *>, std::vector<DataClass *>> handledSubs
                = this->handleSubs(substituteClasses, nonSubstituteClasses);
            int oldSubsClassesSize = substituteClasses.size();
            substituteClasses = std::get<0>(handledSubs);
            if (substituteClasses.size() < oldSubsClassesSize) carryOn = true;
            std::vector<DataClass *> newNoSubsClasses = std::get<1>(handledSubs);
            std::tuple<std::vector<DataClass *>, std::vector<DataClass *>> newGeneratedGeneratedClasses
                    = this->handleTransformationsWithoutSubs(newNoSubsClasses);
            std::vector<DataClass *> newNewSubsClasses = std::get<0>(newGeneratedGeneratedClasses);
            substituteClasses.insert(substituteClasses.end(), newNewSubsClasses.begin(), newNewSubsClasses.end());
            std::vector<DataClass *> newNewNoSubsClasses = std::get<1>(newGeneratedGeneratedClasses);
            nonSubstituteClasses.insert(nonSubstituteClasses.end(), newNewNoSubsClasses.begin(), newNewNoSubsClasses.end());
        } while (carryOn);
        if (!substituteClasses.empty()) {
            std::cerr << "Some substitutions have not been executed" << std::endl;
        }
        return nonSubstituteClasses;
    }
};
