#include <map>
#include "../domain/DataRepresentation.cpp"
#include "../methods/GetterSetterGenerator.cpp"
#include "../methods/ConstructorGenerator.cpp"
#include "../transformation/Transformation.cpp"

class ClassViewGenerator : public ITransformation {
private:
    const std::string allClassViewsAnnotationValue = "*";
    const std::string classViewAnnotationPrefix = "class_view";
    const std::string classViewFieldAnnotationPrefix = "class_view_field";
    GetterSetterGenerator getterSetterGenerator;
    ConstructorGenerator constructorGenerator;

    bool isClassViewAnnotation(Annotation *annotation) {
        return annotation->tokens[0] == classViewAnnotationPrefix;
    }

    bool isClassViewFieldAnnotation(Annotation *annotation) {
        return annotation->tokens[0] == classViewFieldAnnotationPrefix;
    }

    bool isClassViewAllFieldsAnnotation(Annotation *annotation) {
        return annotation->tokens[1] == allClassViewsAnnotationValue;
    }

    std::vector<std::string> parseClassViewFieldNames(Annotation *annotation) {
        std::vector<std::string> names;
        names.insert(names.end(), annotation->tokens.begin() + 1, annotation->tokens.end());
        return names;
    }

    std::set<std::string> getClassViewNames(DataClass *dataClass) {
        std::set<std::string> classViewNamesSet;
        for (auto field: dataClass->fields) {
            std::set<std::string> classViewFieldNames = this->getClassFieldViewNames(field,std::set<std::string>{});
            classViewNamesSet.insert(classViewFieldNames.begin(), classViewFieldNames.end());
        }
        return classViewNamesSet;
    }

    std::set<std::string> getClassFieldViewNames(ClassField *classField, std::set<std::string> allClassViewNames) {
        std::set<std::string> classViewNamesSet;
        for (auto annotation: classField->annotations) {
            if (!isClassViewFieldAnnotation(annotation)) continue;
            if (isClassViewAllFieldsAnnotation(annotation)) return allClassViewNames;

            std::vector<std::string> names = this->parseClassViewFieldNames(annotation);
            classViewNamesSet.insert(names.begin(), names.end());
        }
        return classViewNamesSet;
    }

    DataClass *generateBaseDataClass(DataClass *sourceDataClass) {
        DataClass *baseClass = new DataClass();
        baseClass->name = sourceDataClass->name;
        for (auto field : sourceDataClass->fields) {
            ClassField *baseClassField = field->copy();
            baseClassField->parentClass = baseClass;
            baseClass->fields.emplace_back(baseClassField);
        }
        baseClass->namespaces = sourceDataClass->namespaces;
        baseClass->parameters = sourceDataClass->parameters;
        baseClass->superClasses = sourceDataClass->superClasses;
        return baseClass;
    }

public:
    std::vector<DataClass*> generateClassViews(DataClass *dataClass) {
        std::set<std::string> classViewNames = getClassViewNames(dataClass);
        std::map<std::string, DataClass *> classViewsMap;
        for (const auto& classViewName : classViewNames) {
            auto classView = new DataClass();
            classView->namespaces = dataClass->namespaces;
            classView->superClasses = dataClass->superClasses;
            classView->name = classViewName + dataClass->name;
            classViewsMap[classViewName] = classView;
        }
        for (auto field : dataClass->fields) {
            for (const auto& classViewField : getClassFieldViewNames(field, classViewNames)) {
                ClassField *newClassField = field->copy();
                newClassField->parentClass = classViewsMap[classViewField];
                classViewsMap[classViewField]->fields.emplace_back(newClassField);
            }
        }
        std::vector<DataClass*> classViews;
        classViews.reserve(classViewsMap.size());
        for (auto & it : classViewsMap) {
            classViews.emplace_back(getterSetterGenerator.generateGettersSetters(it.second));
        }
        for (auto classView : classViews) {
            classView->constructors.emplace_back(constructorGenerator.generateNoArgsConstructor(classView));
            classView->constructors.emplace_back(constructorGenerator.generateAllArgsConstructor(classView));
            classView->constructors.emplace_back(constructorGenerator.generateCopyConstructor(classView));
        }
        //TODO error handling - what to do with empty class views?
        return classViews;
    }

    ClassMethod *generateLoaderMethod(DataClass *targetClass, DataClass *inputClass) {
        ClassConstructor *copyConstructor = constructorGenerator.generateCopyConstructor(targetClass, inputClass);
        ClassMethod *loaderMethod = new ClassMethod();
        loaderMethod->accessModifier = AccessModifier::PUBLIC;
        loaderMethod->returnType = new PrimitiveData(PrimitiveDataType::VOID);
        loaderMethod->name = "load";
        loaderMethod->methodArguments = copyConstructor->methodArguments;
        loaderMethod->methodStatements = copyConstructor->methodStatements;
        return loaderMethod;
    }

    bool supports(DataClass *dataClass) override {
        for (auto annotation : dataClass->annotations) {
            if (!this->isClassViewAnnotation(annotation)) continue;
            return true;
        }
        return false;
    }

    std::vector<DataClass *> transform(DataClass *dataClass) override {
        std::vector<DataClass *> generatedViews;
        dataClass = this->generateBaseDataClass(dataClass);
        dataClass = getterSetterGenerator.generateGettersSetters(dataClass);
        generatedViews.emplace_back(dataClass);
        dataClass->constructors.emplace_back(constructorGenerator.generateNoArgsConstructor(dataClass));
        dataClass->constructors.emplace_back(constructorGenerator.generateAllArgsConstructor(dataClass));
        dataClass->constructors.emplace_back(constructorGenerator.generateCopyConstructor(dataClass));
        std::vector<DataClass *> classViews = this->generateClassViews(dataClass);
        generatedViews.insert(generatedViews.end(), classViews.begin(), classViews.end());
        for (auto classViewA : generatedViews) {
            for (auto classViewB : generatedViews) {
                if (classViewA == classViewB) continue;
                classViewA->methods.emplace_back(this->generateLoaderMethod(classViewA, classViewB));
            }
        }
        return generatedViews;
    }
};
