#include <fstream>
#include "../domain/SourceUnit.cpp"

#include "filesystem"

class SourceUnitWriter {
private:
    std::string getPath(const std::vector<std::string>& folderNames) {
        std::string path;
        for (const auto& folderName : folderNames) {
            path += folderName + "/";
        }
        if (path.length() > 1) path.pop_back(); // remote the trailing '/'
        return path;
    }

public:
    void writeSourceUnit(const std::vector<std::string>& prefixPath, SourceUnit *unit) {
        std::vector<std::string> path;
        path.insert(path.end(), prefixPath.begin(), prefixPath.end());
        path.insert(path.end(), unit->path.begin(), unit->path.end());
        std::string pathStr = this->getPath(path);
        std::filesystem::create_directories(std::filesystem::path(pathStr));
        auto fileStream = std::ofstream(pathStr + "/" + unit->name);
        fileStream << unit->contents;
        fileStream.flush();
        fileStream.close();
    }

    void writeSourceUnits(const std::vector<std::string>& prefixPath, const std::vector<SourceUnit *>& units) {
        for (auto source : units) {
            this->writeSourceUnit(prefixPath, source);
        }
    }
};
