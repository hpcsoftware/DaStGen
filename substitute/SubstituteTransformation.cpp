#include <string>
#include <vector>
#include "../domain/DataRepresentation.cpp"
#include "../domain/HighLevelFeatures.cpp"

class SubstituteTypeAnnotationHandler {
private:
    const std::string substituteAnnotation = "substituted";
    const std::string substituteTypeAnnotation = "substitute_with";

    bool isSubstituteFieldAnnotation(Annotation *annotation) {
        if (annotation->parentField->kind != AnnotatedType::Field) return false;
        bool isSubstituteAnnotation = !annotation->tokens.empty() && annotation->tokens[0] == substituteTypeAnnotation;
        return isSubstituteAnnotation;
    }

    bool isSubstitutionField(ClassField *classField) {
        for (auto annotation : classField->annotations) {
            if (!this->isSubstituteFieldAnnotation(annotation)) continue;
            return true;
        }
        return false;
    }

    std::string getSubstitutionClassName(ClassField *classField) {
        for (auto annotation : classField->annotations) {
            if (!this->isSubstituteFieldAnnotation(annotation)) continue;
            return annotation->tokens[1];
        }
        std::cerr << "Field is not a substitution field" << std::endl;
        return "";
    }

    DataClass *getSubstituteDataClass(std::string className, const std::vector<DataClass *>& substituteCandidates) {
        for (auto dataClass : substituteCandidates) {
            if (className == dataClass->name) {
                return dataClass;
            }
        }
        return nullptr;
    }

    IDataType *getNewDataType(IDataType *oldType, DataClass *targetClass) {
        if (dynamic_cast<ReferenceDataType *>(oldType) != nullptr) {
            ReferenceDataType *oldRefType = reinterpret_cast<ReferenceDataType *>(oldType);
            return new ReferenceDataType(this->getNewDataType(oldRefType->getTarget(), targetClass));
        }
        if (dynamic_cast<PointerDataType *>(oldType) != nullptr) {
            PointerDataType *oldRefType = reinterpret_cast<PointerDataType *>(oldType);
            return new PointerDataType(this->getNewDataType(oldRefType->getTarget(), targetClass));
        }
        if (dynamic_cast<ConstSizeArrayDataType *>(oldType) != nullptr) {
            ConstSizeArrayDataType *oldArrType = reinterpret_cast<ConstSizeArrayDataType *>(oldType);
            return new ConstSizeArrayDataType(this->getNewDataType(oldArrType->getItemType(), targetClass), oldArrType->getItemCount());
        }
        if (dynamic_cast<ListDataType*>(oldType) != nullptr) {
            ListDataType *oldListType = reinterpret_cast<ListDataType *>(oldType);
            return new ListDataType(this->getNewDataType(oldListType->getItemType(), targetClass));
        }
        return new DataClassDataType(targetClass);
    }

    bool isSubstituteAnnotation(Annotation *annotation) {
        if (!annotation->tokens.empty() && annotation->tokens[0] == substituteAnnotation)
            return true;
        return false;
    }

public:
    bool isSupported(DataClass *dataClass) {
        for (auto annotation : dataClass->annotations) {
            if (this->isSubstituteAnnotation(annotation)) return true;
        }
        return false;
    }

    bool handle(DataClass *sourceDataClass, const std::vector<DataClass *>& substituteCandidates) {
        for (auto field : sourceDataClass->fields) {
            if (!this->isSubstitutionField(field)) continue;
            std::string substitutionClassName = this->getSubstitutionClassName(field);
            DataClass *substitutionClass = this->getSubstituteDataClass(substitutionClassName, substituteCandidates);
            // only proceed when all substitutions can be done, no partial substitutions are supported
            if (substitutionClass == nullptr) return false;
        }
        for (auto field : sourceDataClass->fields) {
            if (!this->isSubstitutionField(field)) continue;
            std::string substitutionClassName = this->getSubstitutionClassName(field);
            DataClass *substitutionClass = this->getSubstituteDataClass(substitutionClassName, substituteCandidates);
            IDataType *newType = this->getNewDataType(field->type, substitutionClass);
            field->type = newType;
        }
        for (long i = sourceDataClass->annotations.size() -1; i >= 0; i--) {
            if (!this->isSubstituteAnnotation(sourceDataClass->annotations[i])) continue;
            sourceDataClass->annotations.erase(sourceDataClass->annotations.begin() + i);
        }
        return true;
    }

};
