#include "IFieldCompressor.cpp"
#include "../BitsizeDataTypeResolver.cpp"
#include <cmath>

struct FloatFormat {
    std::string sign;
    unsigned int exponentBits;
    unsigned int bias;
    unsigned int significandBits;
};

class FloatFieldCompressor : public IFieldCompressor {
private:

    const std::string floatStandard = "float_standard";
    const std::string floatStandardPrefix = "float";
    const std::string float16 = floatStandardPrefix + "16";
    const std::string float32 = floatStandardPrefix + "32";
    const std::string float64 = floatStandardPrefix + "64";
    const std::string float128 = floatStandardPrefix + "128";


    const std::string floatFormat = "float_format";
    const std::string signPositive = "positive";
    const std::string signNegative = "negative";
    const std::string signMixed = "mixed_sign";

    BitsizeDataTypeResolver bitsizeDataTypeResolver;

    FloatFormat binary16 {
        .sign = signMixed,
        .exponentBits = 5,
        .bias = 15,
        .significandBits = 10
    };

    FloatFormat binary32 {
            .sign = signMixed,
            .exponentBits = 8,
            .bias = 127,
            .significandBits = 23
    };

    FloatFormat binary64 {
            .sign = signMixed,
            .exponentBits = 11,
            .bias = 1023,
            .significandBits = 52
    };

    FloatFormat binary128 {
            .sign = signMixed,
            .exponentBits = 15,
            .bias = 16383,
            .significandBits = 112
    };

    FloatFormat *resolveSourceFormat(ClassField *classField) {
        if (dynamic_cast<PrimitiveData *>(classField->type) == nullptr) {
            std::cerr << "Cannot parse float type of a non-primitive class field" << std::endl;
            return nullptr;
        }
        PrimitiveDataType fieldType = reinterpret_cast<PrimitiveData *>(classField->type)->getDataType();
        switch (fieldType) {
            case FLOAT:
                return &binary32;
            case DOUBLE:
                return &binary64;
            case LONG_DOUBLE:
                return &binary128;
            default:
                std::cerr << "Cannot parse float type of a primitive non-float class field" << std::endl;
                return nullptr;
        }
    }

    bool isFloatStandardAnnotation(Annotation *annotation) {
        bool isFloatStandard = !annotation->tokens.empty() && annotation->tokens[0] == floatStandard;
        return isFloatStandard;
    }

    bool isFloatStandard(ClassField *classField) {
        for (auto annotation : classField->annotations) {
            if (this->isFloatStandardAnnotation(annotation)) return true;
        }
        return false;
    }

    bool isFloatFormat(ClassField *classField) {
        for (auto annotation : classField->annotations) {
            if (this->isFloatFormatAnnotation(annotation)) return true;
        }
        return false;
    }

    FloatFormat *parseFloatStandard(ClassField *classField) {
        Annotation *annotation = nullptr;
        for (auto a : classField->annotations) {
            if (!this->isFloatStandardAnnotation(a)) continue;
            annotation = a;
            break;
        }
        if (annotation == nullptr) {
            std::cerr << "Failed to parse float standard" << std::endl;
            return nullptr;
        }
        std::string standardCandidate = annotation->tokens[1];
        if (standardCandidate == float16) {
            return &binary16;
        }
        if (standardCandidate == float32) {
            return &binary32;
        }
        if (standardCandidate == float64) {
            return &binary64;
        }
        if (standardCandidate == float128) {
            return &binary128;
        }
        std::cerr << "Unknown standard float format" << std::endl;
        return nullptr;
    }

    bool isFloatFormatAnnotation(Annotation *annotation) {
        bool isFloatFormat = !annotation->tokens.empty() && annotation->tokens[0] == floatFormat;
        return isFloatFormat;
    }

    FloatFormat *parseCustomFormat(ClassField *classField) {
        Annotation *annotation = nullptr;
        for (auto a : classField->annotations) {
            if (!this->isFloatFormatAnnotation(a)) continue;
            annotation = a;
            break;
        }
        if (annotation == nullptr) {
            std::cerr << "Failed to parse float format" << std::endl;
            return nullptr;
        }
        std::string signCandidate = annotation->tokens[1];
        if (signCandidate != signPositive && signCandidate != signNegative && signCandidate != signMixed) {
            std::cerr << "Unknown sign value, possible values are " << signPositive << " " << signNegative << " " << signMixed << std::endl;
            return nullptr;
        }

        unsigned int exponentBits = std::stoi(annotation->tokens[2]);
        unsigned int exponentBias = std::stoi(annotation->tokens[3]);
        unsigned int significandBits = std::stoi(annotation->tokens[4]);
        return new FloatFormat{
            .sign = signMixed,
            .exponentBits = exponentBits,
            .bias = exponentBias,
            .significandBits = significandBits
        };
    }

    FloatFormat *getTargetFloatFormat(ClassField *classField) {
        FloatFormat *floatFormat = nullptr;
        if (this->isFloatStandard(classField)) {
            floatFormat = this->parseFloatStandard(classField);
        } else if (this->isFloatFormat(classField)) {
            floatFormat = this->parseCustomFormat(classField);
        }
        return floatFormat;
    }

    int getSize(FloatFormat *floatFormat) {
        int size = 0;
        if (floatFormat->sign == signMixed) size += 1;
        size += floatFormat->exponentBits;
        size += floatFormat->significandBits;
        return size;
    }

    IExpression *getSignificandExpr(FloatFormat *sourceFormat, FloatFormat *targetFormat, IExpression *rawVal) {
        IExpression *shiftExpr = nullptr;
        int newBitsOffset = targetFormat->significandBits - sourceFormat->significandBits;
        if (newBitsOffset >= 0) {
            shiftExpr = new BitshiftLeft(rawVal, new LiteralValue(newBitsOffset));
        } else {
            shiftExpr = new BitshiftRight(rawVal, new LiteralValue(-newBitsOffset));
        }
        unsigned long mask = (1 << targetFormat->significandBits) - 1;
        shiftExpr = new BitwiseAnd(new IsolatedEvaluation(shiftExpr), new ExpressionCast(UNSIGNED_LONG, new LiteralValue(mask)));
        shiftExpr = new IsolatedEvaluation(shiftExpr);
        return shiftExpr;
    }

    IExpression *getExponentExpr(FloatFormat *sourceFormat, FloatFormat *targetFormat, IExpression *rawVal) {
        IExpression *expOnlyExpr = new BitshiftRight(rawVal, new LiteralValue(sourceFormat->significandBits));
        expOnlyExpr = new IsolatedEvaluation(expOnlyExpr);
        unsigned int mask = (1 << sourceFormat->exponentBits) - 1;
        expOnlyExpr = new BitwiseAnd(expOnlyExpr, new LiteralValue(mask));
        expOnlyExpr = new ExpressionCast(UNSIGNED_LONG, expOnlyExpr);
        int exponentBiasDiff = ((int)targetFormat->bias) - sourceFormat->bias;
        expOnlyExpr = new IsolatedEvaluation(new Addition(expOnlyExpr, new LiteralValue(exponentBiasDiff)));
        expOnlyExpr = new BitshiftLeft(expOnlyExpr, new LiteralValue(targetFormat->significandBits));
        expOnlyExpr = new IsolatedEvaluation(expOnlyExpr);
        return expOnlyExpr;
    }

    IExpression *getSignExpr(FloatFormat *sourceFormat, FloatFormat *targetFormat, IExpression *rawVal) {
        if (targetFormat->sign == signPositive || sourceFormat->sign == signPositive) return new LiteralValue(0);
        if (targetFormat->sign == signNegative || sourceFormat->sign == signNegative)
            return new LiteralValue(1UL << (targetFormat->exponentBits + targetFormat->significandBits));
        IExpression *shiftExpr = nullptr;
        //positive to the left
        int newBitsOffset = targetFormat->exponentBits
                + targetFormat->significandBits
                - sourceFormat->exponentBits
                - sourceFormat->significandBits;
        if (newBitsOffset >= 0) {
            shiftExpr = new BitshiftLeft(rawVal, new LiteralValue(newBitsOffset));
        } else {
            shiftExpr = new BitshiftRight(rawVal, new LiteralValue(-newBitsOffset));
        }
        unsigned long mask = 1UL << (targetFormat->exponentBits + targetFormat->significandBits);
        shiftExpr = new BitwiseAnd(new IsolatedEvaluation(shiftExpr), new ExpressionCast(UNSIGNED_LONG, new LiteralValue(mask)));
        shiftExpr = new IsolatedEvaluation(shiftExpr);
        return shiftExpr;
    }

public:

    bool isSupported(ClassField *classField) override {
        bool isSupportedType = false;
        if (dynamic_cast<PrimitiveData*>(classField->type) == nullptr) return false;
        PrimitiveData *dataType = reinterpret_cast<PrimitiveData*>(classField->type);
        switch (dataType->getDataType()) {
            case FLOAT:
            case DOUBLE:
            case LONG_DOUBLE:
                isSupportedType = true;
                break;
            default:
                break;
        }

        if (!isSupportedType) return false;

        if (this->isFloatFormat(classField)) return true;
        if (this->isFloatStandard(classField)) return true;

        return false;
    }

    int getCompressedSize(ClassField *classField) override {
        FloatFormat *floatFormat = this->getTargetFloatFormat(classField);
        int size = this->getSize(floatFormat);
        return size;
    }

    IExpression *getCompressedValExpr(ClassField *compressedField, IExpression *rawVal) override {
        PrimitiveData *dataType = reinterpret_cast<PrimitiveData *>(compressedField->type);
        int typeSize = bitsizeDataTypeResolver.getSize(dataType->getDataType());
        PrimitiveDataType containerType = bitsizeDataTypeResolver.resolveDataType(typeSize);
        rawVal = new ReinterpretCast(new ReferenceDataType(new PrimitiveData(containerType)), rawVal);
        FloatFormat *sourceFormat = this->resolveSourceFormat(compressedField);
        FloatFormat *targetFormat = this->getTargetFloatFormat(compressedField);
        IExpression *significand = this->getSignificandExpr(sourceFormat, targetFormat, rawVal);
        IExpression *exponent = this->getExponentExpr(sourceFormat, targetFormat, rawVal);
        IExpression *sign = this->getSignExpr(sourceFormat, targetFormat, rawVal);
        return new BitwiseOr(sign, new BitwiseOr(exponent, significand));
    };

    IExpression *getDecompressedValExpr(ClassField *compressedField, IExpression *compressedVal) override {
        FloatFormat *sourceFormat = this->getTargetFloatFormat(compressedField);
        FloatFormat *targetFormat = this->resolveSourceFormat(compressedField);
        IExpression *significand = this->getSignificandExpr(sourceFormat, targetFormat, compressedVal);
        IExpression *exponent = this->getExponentExpr(sourceFormat, targetFormat, compressedVal);
        IExpression *sign = this->getSignExpr(sourceFormat, targetFormat, compressedVal);
        return new BitwiseOr(sign, new BitwiseOr(exponent, significand));
    };
};