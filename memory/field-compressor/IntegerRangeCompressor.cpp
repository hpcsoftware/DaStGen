#include "IFieldCompressor.cpp"
#include <cmath>

class IntegerRangeFieldCompressor : public IFieldCompressor {
private:
    const std::string intRangeAnnotation = "int_range";

    long getRangeLowerBound(ClassField *classField) {
        std::string lowerBoundStr;
        for (auto annotation : classField->annotations) {
            if (annotation->tokens[0] != intRangeAnnotation) continue;
            lowerBoundStr = annotation->tokens[1];
        }
        if (lowerBoundStr.empty()) {
            std::cerr << "Integer range lower bound is not specified" << std::endl;
            return 0;
        }
        long lowerBound = std::stol(lowerBoundStr);
        return lowerBound;
    }

    unsigned long getRangeUpperBound(ClassField *classField) {
        std::string upperBoundStr;
        for (auto annotation : classField->annotations) {
            if (annotation->tokens[0] != intRangeAnnotation) continue;
            if (annotation->tokens.size() != 3) {
                std::cerr << "Integer range upper bound is not specified" << std::endl;
                return 0;
            }
            upperBoundStr = annotation->tokens[2];
        }
        if (upperBoundStr.empty()) {
            std::cerr << "Integer range upper bound is not specified" << std::endl;
            return 0;
        }
        unsigned long upperBound = std::stoul(upperBoundStr);
        return upperBound;
    }

public:

    bool isSupported(ClassField *classField) override {
        bool isSupportedType = false;
        if (dynamic_cast<PrimitiveData*>(classField->type) == nullptr) return false;
        PrimitiveData *dataType = reinterpret_cast<PrimitiveData*>(classField->type);
        switch (dataType->getDataType()) {
            case CHAR:
            case UNSIGNED_CHAR:
            case SHORT:
            case UNSIGNED_SHORT:
            case INT:
            case UNSIGNED_INT:
            case LONG:
            case UNSIGNED_LONG:
            case LONG_LONG:
            case UNSIGNED_LONG_LONG:
                isSupportedType = true;
                break;
            default:
                break;
        }
        if (!isSupportedType) return false;

        for (auto annotation : classField->annotations) {
            if (annotation->tokens[0] == intRangeAnnotation) return true;
        }

        return false;
    }

    int getCompressedSize(ClassField *classField) override {
        long lowerBound = this->getRangeLowerBound(classField);
        unsigned long upperBound = this->getRangeUpperBound(classField);
        unsigned long range = upperBound - lowerBound + 1;
        int rangeBits = ceil(log2(range));
        return rangeBits;
    }

    IExpression *getCompressedValExpr(ClassField *compressedField, IExpression *rawVal) override {
        return new Subtraction(rawVal, new LiteralValue(this->getRangeLowerBound(compressedField)));
    };

    IExpression *getDecompressedValExpr(ClassField *compressedField, IExpression *compressedVal) override {
        IExpression *val = new Addition(compressedVal, new LiteralValue(this->getRangeLowerBound(compressedField)));
        return new ExpressionCast(reinterpret_cast<PrimitiveData *>(compressedField->type), val);
    };
};