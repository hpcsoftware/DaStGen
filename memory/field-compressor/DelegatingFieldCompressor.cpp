#include "BoolFieldCompressor.cpp"
#include "IntegerRangeCompressor.cpp"
#include "FloatFieldCompressor.cpp"
#include "EnumFieldCompressor.cpp"
#include "../BitLevelInstructionSetResolver.cpp"

class DelegatingFieldCompressor : public IFieldCompressor {
private:
    std::vector<IFieldCompressor *> compressors {
            new BoolFieldCompressor(),
            new IntegerRangeFieldCompressor(),
            new FloatFieldCompressor(),
            new EnumFieldCompressor()
    };

    IFieldCompressor *getSupportingCompressor(ClassField *field) {
        for (auto compressor : compressors) {
            if (compressor->isSupported(field)) return compressor;
        }
        return nullptr;
    }

public:
    bool isSupported(ClassField *classField) override {
        IFieldCompressor *compressor = getSupportingCompressor(classField);
        if (compressor == nullptr) return false;
        bool isSupported = compressor->isSupported(classField);
        return isSupported;
    }

    int getCompressedSize(ClassField *classField) override {
        return getSupportingCompressor(classField)->getCompressedSize(classField);
    }

    IExpression *getCompressedValExpr(ClassField *compressedField, IExpression *rawVal) override {
        return getSupportingCompressor(compressedField)->getCompressedValExpr(compressedField, rawVal);
    }

    IExpression *getDecompressedValExpr(ClassField *compressedField, IExpression *compressedVal) override {
        return getSupportingCompressor(compressedField)->getDecompressedValExpr(compressedField, compressedVal);
    }
};