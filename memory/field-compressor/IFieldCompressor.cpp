#ifndef __DASTGEN2_FieldCompressor__
#define __DASTGEN2_FieldCompressor__

#include "../../domain/DataRepresentation.cpp"
#include "../../domain/LogicRepresentation.cpp"

class IFieldCompressor {
public:
    virtual bool isSupported(ClassField *classField) { std::cerr << "Override me!" << std::endl; return false; };
    virtual int getCompressedSize(ClassField *classField) { std::cerr << "Override me!" << std::endl; return 0; };

    virtual IExpression *getCompressedValExpr(ClassField *compressedField, IExpression *rawVal) {
        std::cerr << "Override me!" << std::endl; return nullptr;
    };

    virtual IExpression *getDecompressedValExpr(ClassField *compressedField, IExpression *compressedVal) {
        std::cerr << "Override me!" << std::endl; return nullptr;
    };
};

#endif