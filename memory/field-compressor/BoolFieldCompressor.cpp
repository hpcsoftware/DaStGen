#include "IFieldCompressor.cpp"
#include "../../methods/GetterSetterGenerator.cpp"

class BoolFieldCompressor : public IFieldCompressor {

public:
    bool isSupported(ClassField *classField) override {
        if (dynamic_cast<PrimitiveData*>(classField->type) == nullptr) return false;
        PrimitiveData *data = reinterpret_cast<PrimitiveData*>(classField->type);
        return data->equals(PrimitiveDataType::BOOLEAN);
    }

    int getCompressedSize(ClassField *classField) override {
        return 1;
    }

    IExpression *getCompressedValExpr(ClassField *compressedField, IExpression *rawVal) override {
        return rawVal;
    }

    IExpression *getDecompressedValExpr(ClassField *compressedField, IExpression *compressedVal) override {
        return new ExpressionCast(PrimitiveDataType::BOOLEAN, compressedVal);
    }
};