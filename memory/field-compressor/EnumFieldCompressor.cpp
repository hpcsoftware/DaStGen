#ifndef __DASTGEN2_EnumFieldCompressor__
#define __DASTGEN2_EnumFieldCompressor__

#include "../../domain/DataRepresentation.cpp"
#include "./IFieldCompressor.cpp"
#include <cmath>

class EnumFieldCompressor : public IFieldCompressor {
private:

    unsigned int getMinValue(std::vector<EnumValue> values) {
        unsigned int minVal = -1;
        for (auto &value : values) {
            unsigned int val = value.getValue();
            if (val < minVal) minVal = val;
        }
        return minVal;
    }

    unsigned int getMaxVal(std::vector<EnumValue> values) {
        unsigned int maxVal = 0;
        for (auto &value : values) {
            unsigned int val = value.getValue();
            if (val > maxVal) maxVal = val;
        }
        return maxVal;
    }

    bool areValuesContinuous(const std::vector<EnumValue>& values) {
        unsigned int minVal = this->getMinValue(values);
        unsigned int maxVal = this->getMaxVal(values);
        unsigned int range = maxVal - minVal + 1;
        if (range <= values.size()) return true;
        return false;
    }

    IExpression *getCompressedExprForContinuousVals(EnumDataType *enumType, IExpression *rawVal) {
        int minVal = this->getMinValue(enumType->getTargetEnum()->values);
        return new ExpressionCast(enumType, new Subtraction(rawVal, new LiteralValue(minVal)));
    }

    IExpression *getDecompressedExprForContinuousVals(EnumDataType *enumType, IExpression *compressedVal) {
        int minVal = this->getMinValue(enumType->getTargetEnum()->values);
        return new ExpressionCast(enumType, new Addition(compressedVal, new LiteralValue(minVal)));
    }

    IExpression *getCompressedExprForDiscontinuousVals(EnumDataType *enumType, IExpression *rawVal) {
        IExpression *elseVal = new LiteralValue(0);
        int valIndex = 0;
        for (auto &value : enumType->getTargetEnum()->values) {
            IExpression *comparison = new EqualityComparison(rawVal, new LiteralValue(value.getValue()));
            IExpression *trueVal = new LiteralValue(valIndex);
            elseVal = new ConditionalExpression(comparison, trueVal, elseVal);
            valIndex++;
        }
        return new ExpressionCast(enumType, elseVal);
    }

    IExpression *getDecompressedExprForDiscontinuousVals(EnumDataType *enumType, IExpression *compressedVal) {
        IExpression *elseVal = new LiteralValue(0);
        int valIndex = 0;
        for (auto &value : enumType->getTargetEnum()->values) {
            IExpression *comparison = new EqualityComparison(compressedVal, new LiteralValue(valIndex));
            IExpression *trueVal = new LiteralValue(value.getValue());
            elseVal = new ConditionalExpression(comparison, trueVal, elseVal);
            valIndex++;
        }
        elseVal = new ExpressionCast(enumType, elseVal);
        return elseVal;
    }


public:
    bool isSupported(ClassField *classField) override {
        if (dynamic_cast<EnumDataType *>(classField->type) == nullptr) return false;
        EnumDataType *enumDataType = reinterpret_cast<EnumDataType *>(classField->type);
        if (enumDataType->getTargetEnum()->values.size() < 2) return false;
        return true;
    }

    int getCompressedSize(ClassField *classField) override {
        EnumDataType *enumType = dynamic_cast<EnumDataType *>(classField->type);
        int valueCount = enumType->getTargetEnum()->values.size();
        int size = ceil(log2(valueCount));
        return size;
    }

    IExpression *getCompressedValExpr(ClassField *compressedField, IExpression *rawVal) override {
        EnumDataType *enumDataType = reinterpret_cast<EnumDataType *>(compressedField->type);
        if (this->areValuesContinuous(enumDataType->getTargetEnum()->values)) {
            return this->getCompressedExprForContinuousVals(enumDataType, rawVal);
        }
        return this->getCompressedExprForDiscontinuousVals(enumDataType, rawVal);
    }

    IExpression *getDecompressedValExpr(ClassField *compressedField, IExpression *compressedVal) override {
        EnumDataType *enumDataType = reinterpret_cast<EnumDataType *>(compressedField->type);
        if (this->areValuesContinuous(enumDataType->getTargetEnum()->values)) {
            return this->getDecompressedExprForContinuousVals(enumDataType, compressedVal);
        }
        return this->getDecompressedExprForDiscontinuousVals(enumDataType, compressedVal);
    }

};

#endif