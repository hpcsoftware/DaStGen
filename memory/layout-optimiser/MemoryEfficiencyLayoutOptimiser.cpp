#include "AbstractBinPackingLayoutOptimiser.cpp"

class MemoryEfficiencyLayoutOptimiser : public ILayoutOptimiser, AbstractBinPackingLayoutOptimiser {
private:
    static bool sortByMinWastedSpace(const std::vector<LimitedSizeCompressibleField *>& a,
                                     const std::vector<LimitedSizeCompressibleField *>& b) {
        int sizeA = 0;
        for (auto field : a) sizeA += field->limitedSize;

        int sizeB = 0;
        for (auto field : b) sizeB += field->limitedSize;

        return sizeA > sizeB;
    }

    int freeSpaceBudget = 0;
public:

    MemoryEfficiencyLayoutOptimiser() = default;

    explicit MemoryEfficiencyLayoutOptimiser(int _freeSpaceBudget) : freeSpaceBudget(_freeSpaceBudget) {}

    MemoryMap *getCompressedFields(std::vector<CompressibleField *> fieldsList, int blockSize) override {
        std::vector<LimitedSizeCompressibleField *> compressibleFields = this->copyFields(fieldsList, blockSize);
        std::vector<std::vector<LimitedSizeCompressibleField *>> fieldBlocks = this->getFieldBlocks(compressibleFields, blockSize);
        std::vector<std::vector<LimitedSizeCompressibleField *>> fullFieldBlocks;
        std::vector<std::vector<LimitedSizeCompressibleField *>> partialFieldBlocks;

        int blockCount = 0;
        for (const auto& fieldBlock : fieldBlocks) {
            int blockUsage = 0;
            for (auto field : fieldBlock) {
                blockUsage += field->limitedSize;
                blockCount += field->additionalBlocks;
            }
            if (blockUsage == blockSize) fullFieldBlocks.emplace_back(fieldBlock);
            else partialFieldBlocks.emplace_back(fieldBlock);
        }
        blockCount += fullFieldBlocks.size();

        std::vector<CompressibleField *> fields;

        for (const auto& fieldBlock : fullFieldBlocks) {
            for (auto field : fieldBlock) fields.emplace_back(field->field);
        }

        int totalMisalignedBits = 0;
        sort(partialFieldBlocks.begin(), partialFieldBlocks.end(), sortByMinWastedSpace);
        for (const auto& fieldBlock : partialFieldBlocks) {
            for (auto field : fieldBlock) totalMisalignedBits += field->limitedSize;
        }

        int misalignedBlocksCount = ceil((double) totalMisalignedBits / blockSize);
        int freeSpareBits = misalignedBlocksCount * blockSize - totalMisalignedBits;
        int totalSpareBits = freeSpareBits + this->freeSpaceBudget;
        int usedSpareBits = 0;

        for (const auto& fieldBlock : partialFieldBlocks) {
            int blockUsage = 0;
            for (auto field : fieldBlock) {
                fields.emplace_back(field->field);
                blockUsage += field->limitedSize;
            }
            int spareSpace = blockSize - blockUsage;
            if (spareSpace > totalSpareBits) continue;
            totalSpareBits -= spareSpace;
            usedSpareBits += spareSpace;
            fields.emplace_back(new CompressibleField(nullptr, spareSpace));
        }

        usedSpareBits -= freeSpareBits;
        if (usedSpareBits < 0) usedSpareBits = 0;
        blockCount += ceil((double) (totalMisalignedBits + usedSpareBits) / blockSize);

        MemoryMap *map = new MemoryMap();
        map->blockSize = blockSize;
        map->blockCount = blockCount;
        map->fields = fields;
        return map;
    }
};
