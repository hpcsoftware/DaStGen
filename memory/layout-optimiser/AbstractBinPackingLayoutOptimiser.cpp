#ifndef __DASTGEN2_AbstractLayoutOptimiser__
#define __DASTGEN2_AbstractLayoutOptimiser__

#include "ILayoutOptimiser.cpp"

#include "ortools/linear_solver/linear_expr.h"
#include "ortools/linear_solver/linear_solver.h"

struct LimitedSizeCompressibleField {
    CompressibleField *field;
    int limitedSize;
    int additionalBlocks;
};

class AbstractBinPackingLayoutOptimiser {

protected:
    std::vector<std::vector<LimitedSizeCompressibleField *>> getFieldBlocks(std::vector<LimitedSizeCompressibleField *> fields, int blockSize) {
        int fieldCount = fields.size();
        int blockCount = fieldCount;

        operations_research::MPSolver solver("",
                                             operations_research::MPSolver::SCIP_MIXED_INTEGER_PROGRAMMING);

        std::vector<std::vector<const operations_research::MPVariable *>> fieldBlockMatrix(
                fieldCount, std::vector<const operations_research::MPVariable *>(blockCount));
        for (int fieldIndex = 0; fieldIndex < fieldCount; fieldIndex++) {
            for (int blockIndex = 0; blockIndex < blockCount; blockIndex++) {
                fieldBlockMatrix[fieldIndex][blockIndex] = solver.MakeIntVar(0.0, 1.0, "");
            }
        }

        std::vector<const operations_research::MPVariable *> blockUsageVector(blockCount);
        for (int blockIndex = 0; blockIndex < blockCount; blockIndex++) {
            blockUsageVector[blockIndex] = solver.MakeIntVar(0.0, 1.0, "");
        }

        for (int blockIndex = 0; blockIndex < blockCount; ++blockIndex) {
            operations_research::LinearExpr weight;
            for (int fieldIdex = 0; fieldIdex < fieldCount; fieldIdex++) {
                weight += fields[fieldIdex]->limitedSize * operations_research::LinearExpr(fieldBlockMatrix[fieldIdex][blockIndex]);
            }
            solver.MakeRowConstraint(weight <= operations_research::LinearExpr(blockUsageVector[blockIndex]) * blockSize);
        }

        for (int fieldIndex = 0; fieldIndex < fieldCount; fieldIndex++) {
            operations_research::LinearExpr sum;
            for (int blockIndex = 0; blockIndex < blockCount; blockIndex++) {
                sum += fieldBlockMatrix[fieldIndex][blockIndex];
            }
            solver.MakeRowConstraint(sum == 1.0);
        }


        // Create the objective function.
        operations_research::MPObjective *const objective = solver.MutableObjective();
        operations_research::LinearExpr num_bins_used;
        for (int blockIndex = 0; blockIndex < blockCount; blockIndex++) {
            num_bins_used += blockUsageVector[blockIndex];
        }
        objective->MinimizeLinearExpr(num_bins_used);

        const operations_research::MPSolver::ResultStatus result_status = solver.Solve();

        // Check that the problem has an optimal solution.
        if (result_status != operations_research::MPSolver::OPTIMAL) {
            std::cerr << "The problem does not have an optimal solution!";
            std::vector<std::vector<CompressibleField *>>();
        }

        std::vector<std::vector<LimitedSizeCompressibleField *>> fieldBlocks;

        for (int blockIndex = 0; blockIndex < blockCount; blockIndex++) {
            if (blockUsageVector[blockIndex]->solution_value() == 0) continue;
            std::vector<LimitedSizeCompressibleField *> fieldBlock;
            for (int fieldIndex = 0; fieldIndex < fieldCount; fieldIndex++) {
                if (fieldBlockMatrix[fieldIndex][blockIndex]->solution_value() == 0) continue;
                fieldBlock.emplace_back(fields[fieldIndex]);
            }
            fieldBlocks.emplace_back(fieldBlock);
        }

        return fieldBlocks;
    }

    std::vector<LimitedSizeCompressibleField *> copyFields(const std::vector<CompressibleField *>& fields, int blockSize) {
        std::vector<LimitedSizeCompressibleField *> copy;
        copy.reserve(fields.size());
        for (auto f : fields) {
            auto lField = new LimitedSizeCompressibleField();
            lField->field = f;
            lField->additionalBlocks = f->getSize() / blockSize;
            lField->limitedSize = f->getSize() % blockSize;
            if (lField->limitedSize == 0) {
                lField->limitedSize = blockSize;
                lField->additionalBlocks--; //additional blocks is never zero here
            }
            copy.emplace_back(lField);
        }
        return copy;
    }


};

#endif