#include "AbstractBinPackingLayoutOptimiser.cpp"

class AccessTimeLayoutOptimiser : public ILayoutOptimiser, AbstractBinPackingLayoutOptimiser {

public:
    MemoryMap *getCompressedFields(std::vector<CompressibleField *> fieldsList, int blockSize) override {
        std::vector<LimitedSizeCompressibleField *> compressibleFields = this->copyFields(fieldsList, blockSize);
        std::vector<std::vector<LimitedSizeCompressibleField *>> fieldBlocks = this->getFieldBlocks(compressibleFields, blockSize);
        std::vector<CompressibleField *> fields;
        int blockCount = fieldBlocks.size();
        for (const auto& fieldBlock : fieldBlocks) {
            int blockUsage = 0;
            for (auto field : fieldBlock) {
                fields.emplace_back(field->field);
                blockUsage += field->limitedSize;
                blockCount += field->additionalBlocks;
            }
            int spareSpace = blockSize - blockUsage;
            if (spareSpace == 0) continue;
            fields.emplace_back(new CompressibleField(nullptr, spareSpace));
        }
        MemoryMap *map = new MemoryMap();
        map->blockSize = blockSize;
        map->blockCount = blockCount;
        map->fields = fields;
        return map;
    }
};
